<?php

namespace App\Http\Controllers;

use App\Models\ApartProperty;
use Illuminate\Http\Request;

class ApartPropertyCRUDController extends CRUDController
{
    const MODEL = ApartProperty::class;
    const TITLE_SINGULAR = 'Özellik';
    const TITLE_PLURAL = 'Apart Özellikleri';


    protected $prefix = 'apartproperty';

    protected function getColumns() {
        return [
            [
                'name' => 'order',
                'label' => 'Sıra',
                'type' => 'text',
                'id'=> 'kt_inputmask_10',
                'colSize' => 6,
            ],
            [
                'name' => 'property',
                'label' => 'Özellik',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
        ];
    }
}
