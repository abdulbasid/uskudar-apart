<?php

namespace App\Http\Controllers;

use App\Models\RoomDet;
use Illuminate\Http\Request;

class RoomDetController  extends Controller
{
    public function index(Request $request)
    { 

        $roomdets = RoomDet::All();
        return view('front.rooms', [
            'roomdets' => $roomdets,
        ]);
    }
}
