<?php

namespace App\Http\Controllers;

use App\Models\LocationItem;
use App\Models\ApartProperty;
use App\Models\Whyapart;
use App\Models\Tenant;
use App\Models\Floor;
use App\Models\Room;
use Illuminate\Http\Request;

class MainPageController  extends Controller
{
    public function index(Request $request)
    {
        $apartproperties = ApartProperty::All();
        $locationitems = LocationItem::All();
        $whyaparts = Whyapart::All();
        $tenantscount = Tenant::All('id')->count();
        $floorscount = Floor::All('id')->count();
        $roomscount = Room::All('id')->count();
        return view('welcome', [
            'locationitems' => $locationitems,
            'apartproperties' => $apartproperties,
            'whyaparts' => $whyaparts,
            'tenantscount' => $tenantscount,
            'floorscount' => $floorscount,
            'roomscount' => $roomscount,
        ]);

       

        return view('welcome');
    }
}