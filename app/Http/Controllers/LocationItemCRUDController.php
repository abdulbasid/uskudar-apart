<?php

namespace App\Http\Controllers;

use App\Models\LocationItem;
use Illuminate\Http\Request;

class LocationItemCRUDController extends CRUDController
{
    const MODEL = LocationItem::class;
    const TITLE_SINGULAR = 'Madde';
    const TITLE_PLURAL = 'Konum Özellikleri';


    protected $prefix = 'locationitem';

    protected function getColumns() {
        return [
            [
                'name' => 'item',
                'label' => 'Maddeler',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
        ];
    }
}
