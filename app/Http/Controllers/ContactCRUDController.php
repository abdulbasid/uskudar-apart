<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

class ContactCRUDController extends CRUDController
{
    const MODEL = Contact::class;
    const TITLE_SINGULAR = 'İletişim';
    const TITLE_PLURAL = 'İletişim';


    protected $prefix = 'contacts';

    protected function getColumns() {
        return [
            [
                'name' => 'phone',
                'label' => 'Telefon',
                'type' => 'text',
                'id'=> 'kt_inputmask_3',
                'colSize' => 6,
            ],
            [
                'name' => 'adress',
                'label' => 'Adres',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'email',
                'label' => 'E-Mail',
                'type' => 'text',
                'id'=> 'kt_inputmask_9',
                'colSize' => 6,
            ],
            [
                'name' => 'title',
                'label' => 'Başlık',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'paragraph',
                'label' => 'Paragraf',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
        ];
    }
}
