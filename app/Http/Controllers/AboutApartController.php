<?php

namespace App\Http\Controllers;

use App\Models\Aboutapart;
use App\Models\RoomDet;
use Illuminate\Http\Request;

class AboutApartController  extends Controller
{
    public function index(Request $request)
    { 

        $aboutaparts = Aboutapart::All();
        $roomdets = RoomDet::All();
        return view('front.about-apartment', [
            'aboutaparts' => $aboutaparts,
            'roomdets' => $roomdets,
        ]);
        return view('front.about-apartment');
    }
}
