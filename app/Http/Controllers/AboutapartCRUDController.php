<?php

namespace App\Http\Controllers;

use App\Models\Aboutapart;
use Illuminate\Http\Request;

class AboutapartCRUDController extends CRUDController
{
    const MODEL = Aboutapart::class;
    const TITLE_SINGULAR = 'Apart Hakkında';
    const TITLE_PLURAL = 'Apart Hakkında';


    protected $prefix = 'aboutapart';

    protected function getColumns() {
        return [
            [
                'name' => 'title',
                'label' => 'Başlık',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'paragraph',
                'label' => 'Paragraf',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'paragraph1',
                'label' => 'Paragraf 1',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'paragraph2',
                'label' => 'Paragraf 2',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
        ];
    }
}
