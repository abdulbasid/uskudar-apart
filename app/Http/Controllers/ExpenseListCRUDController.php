<?php

namespace App\Http\Controllers;

use App\Models\ExpenseList;
use Illuminate\Http\Request;

class ExpenseListCRUDController extends CRUDController
{
    const MODEL = ExpenseList::class;
    const TITLE_SINGULAR = 'Gider Türü';
    const TITLE_PLURAL = 'Gider Türleri';


    protected $prefix = 'expense_lists';

    protected function getColumns() {
        return [
            [
                'name' => 'name',
                'label' => 'Gider Adı',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
        ];
    }
}
