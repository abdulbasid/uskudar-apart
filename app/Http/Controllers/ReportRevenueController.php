<?php

namespace App\Http\Controllers;

use App\Models\Revenue;
use Illuminate\Http\Request;

class ReportRevenueController extends Controller
{
    public function index(Request $request)
    {

    
        $revenue = Revenue::All();
        $revenuesum = Revenue::All('price')->sum('price');
        return view('report.revenue', [
            'revenue' => $revenue,
            'revenuesum' => $revenuesum,
        ]);

        return view('report.revenue');
    }
}
