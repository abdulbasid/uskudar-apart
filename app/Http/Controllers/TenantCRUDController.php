<?php

namespace App\Http\Controllers;

use App\Models\Tenant;
use App\Models\Floor;
use App\Models\Room;
use Illuminate\Http\Request;

class TenantCRUDController extends CRUDController
{
    const MODEL = Tenant::class;
    const TITLE_SINGULAR = 'Kiracı';
    const TITLE_PLURAL = 'Kiracılar';


    protected $prefix = 'tenants';

    protected function getColumns() {
        return [
            [
                'name' => 'tc',
                'label' => 'Tc Kimlik',
                'type' => 'text',
                'id'=> 'kt_inputmask_5',
                'colSize' => 6,
            ],
            [
                'name' => 'name',
                'label' => 'Adı',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'surname',
                'label' => 'Soyadı',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'phone',
                'label' => 'Telefon',
                'type' => 'text',
                'id'=> 'kt_inputmask_3',
                'colSize' => 6,
            ],
            [
                'name' => 'email',
                'label' => 'E-Mail',
                'type' => 'text',
                'id'=> 'kt_inputmask_9',
                'colSize' => 6,
            ],
            [
                'name' => 'job',
                'label' => 'Meslek',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'price',
                'label' => 'Kira Ücreti',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'startdate',
                'label' => 'Giriş Tarihi',
                'type' => 'date',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'enddate',
                'label' => 'Çıkış Tarihi',
                'type' => 'date',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'floor_id',
                'label' => 'Kat',
                'type' => 'join-1n',
                'relation' => 'reference',
                'relation_key' => 'floor',
                'model' => Floor::class,
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'room_id',
                'label' => 'Oda Numarası',
                'type' => 'join-1n',
                'relation' => 'room',
                'relation_key' => 'roomNumber',
                'model' => Room::class,
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'statu',
                'label' => 'Durum',
                'type' => 'list',
                'colSize' => 6,
                'id'=> '',  
                'options' => [
                    'kaliyor' => 'Kalıyor',
                    'ayrildi' => 'Ayrıldı',
                ]
            ],
        ];
    }
}
