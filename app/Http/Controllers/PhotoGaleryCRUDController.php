<?php

namespace App\Http\Controllers;

use App\Models\PhotoGalery;
use Illuminate\Http\Request;

class PhotoGaleryCRUDController extends CRUDController
{
    const MODEL = PhotoGalery::class;
    const TITLE_SINGULAR = 'Fotoğraf';
    const TITLE_PLURAL = 'Fotoğraflar';


    protected $prefix = 'photogalery';

    protected function getColumns() {
        return [
            [
                'name' => 'photo',
                'label' => 'Fotoğraf',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'category',
                'label' => 'Kategori',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
        ];
    }
}
