<?php

namespace App\Http\Controllers;

use App\Models\Whyapart;
use Illuminate\Http\Request;

class WhyapartCRUDController extends CRUDController
{
    const MODEL = Whyapart::class;
    const TITLE_SINGULAR = 'Bilgi';
    const TITLE_PLURAL = 'Neden Üsküdar Apart';


    protected $prefix = 'whyapart';

    protected function getColumns() {
        return [
            [
                'name' => 'title',
                'label' => 'Başlık',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'paragraph',
                'label' => 'Paragraf',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'photo',
                'label' => 'Fotoğraf',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
        ];
    }
}
