<?php

namespace App\Http\Controllers;

use App\Models\Floor;
use Illuminate\Http\Request;

class FloorCRUDController extends CRUDController
{
    const MODEL = Floor::class;
    const TITLE_SINGULAR = 'Kat';
    const TITLE_PLURAL = 'Katlar';


    protected $prefix = 'floors';

    protected function getColumns() {
        return [
            [
                'name' => 'floor',
                'label' => 'Kat Sırası',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
        ];
    }
}
