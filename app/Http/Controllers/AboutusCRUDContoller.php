<?php

namespace App\Http\Controllers;

use App\Models\Aboutus;
use Illuminate\Http\Request;

class AboutusCRUDContoller extends CRUDController
{
    const MODEL = Aboutus::class;
    const TITLE_SINGULAR = 'Hakkımızda';
    const TITLE_PLURAL = 'Hakkımızda';


    protected $prefix = 'aboutus';

    protected function getColumns() {
        return [
            [
                'name' => 'title',
                'label' => 'Başlık',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'paragraph1',
                'label' => 'Paragraf 1',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'paragraph2',
                'label' => 'Paragraf 2',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'photo',
                'label' => 'Fotoğraf',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
        ];
    }
}
