<?php

namespace App\Http\Controllers;

use App\Models\Room;
use Illuminate\Http\Request;
use App\Models\Floor;

class RoomCRUDController extends CRUDController
{
    const MODEL = Room::class;
    const TITLE_SINGULAR = 'Oda';
    const TITLE_PLURAL = 'Odalar';


    protected $prefix = 'rooms';

    protected function getColumns() {
        return [
            [
                'name' => 'floor_id',
                'label' => 'Kat',
                'type' => 'join-1n',
                'relation' => 'reference',
                'relation_key' => 'floor',
                'model' => Floor::class,
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'number',
                'label' => 'Oda Numarası',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'roomCount',
                'label' => 'Toplam Kişi Sayısı',
                'type' => 'number',
                'id'=> 'kt_inputmask_10',
                'colSize' => 6,
            ],
            [
                'name' => 'remaining_number',
                'label' => 'Oda da Kalan Sayısı',
                'type' => 'text',
                'id'=> 'kt_inputmask_10',
                'colSize' => 6,
            ],
            
        ];
    }
}
