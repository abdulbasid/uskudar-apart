<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\Revenue;
use App\Models\Tenant;

class CRUDController extends Controller
{
    public function index() {
        return view('crud.index', [
            'singularTitle' => $this::TITLE_SINGULAR,
            'pluralTitle' => $this::TITLE_PLURAL,
            'createRoute' => route("{$this->prefix}.create"),
            'rowActions' => [$this, 'rowActions'],
            'columns' => $this->getColumns(),
            'rows' => ($this::MODEL)::all(),
        ]);
    }

    public function show($link_id) {

        $tenantId = Tenant::All('id');
        $revenueNames= Revenue::All()->where('tenant_id', $tenantId);
        
        $revenues = Revenue::All()->where('id', $link_id);
        $model = ($this::MODEL)::findOrFail($link_id);
        return view('crud.show', [
            'singularTitle' => $this::TITLE_SINGULAR,
            'pluralTitle' => $this::TITLE_PLURAL,
            'columns' => $this->getColumns(),
            'row' => $model,
            'revenues' => $revenues,
            'revenueNames' => $revenueNames,
            
        ]);
    }

    public function edit($link_id) {
        $model = ($this::MODEL)::findOrFail($link_id);
        return view('crud.edit', [
            'updateRoute' => "{$this->prefix}.update",
            'singularTitle' => $this::TITLE_SINGULAR,
            'pluralTitle' => $this::TITLE_PLURAL,
            'columns' => $this->getColumns(),
            'row' => $model,
        ]);
    }

    public function update($link_id, Request $request) {
        $model = ($this::MODEL)::findOrFail($link_id);
        $save_state = false;
        foreach ($this->getColumns() as $column) {
            $name = $column['name'];
            $value = $request->input($name);
            if ($model->$name != $value) {
                $save_state = true;
                $model->$name = $value;
            }
        }
        if ($save_state)
            $model->save();
        return redirect()->route("{$this->prefix}.index");
    }

    public function create() {
        return view('crud.create', [
            'singularTitle' => $this::TITLE_SINGULAR,
            'pluralTitle' => $this::TITLE_PLURAL,
            'columns' => $this->getColumns(),
            'storeRoute' => route("{$this->prefix}.store"),
        ]);
    }

    public function store(Request $request) {
        $modelClass = $this::MODEL;
        $model = new $modelClass;
        foreach ($this->getColumns() as $column) {
            $name = $column['name'];
            
            if($name=="password"){
                $model->$name = Hash::make($request->input($name));
            }else{
                $model->$name = $request->input($name);
            }
                
            
        }
        $model->save();
        return redirect()->route("{$this->prefix}.index");
    }

    public function destroy($link_id) {
        $model = ($this::MODEL)::findOrFail($link_id);
        $model->delete();
        return redirect()->route("{$this->prefix}.index");
    }

    public function rowActions($row)
    {
        return [
            [
                'label' => 'İncele',
                'type' => 'accent',
                'action' => route("{$this->prefix}.show", $row->id),
                'method' => 'GET',
                'icon' => 'eye',
            ],
            [
                'label' => 'Düzenle',
                'type' => 'primary',
                'action' => route("{$this->prefix}.edit", $row->id),
                'method' => 'GET',
                'icon' => 'pen',
            ],
            [
                'label' => 'Sil',
                'type' => 'danger',
                'action' => route("{$this->prefix}.show", $row->id),
                'method' => 'DELETE',
                'icon' => 'trash',
            ],
        ];
    }
}
