<?php

namespace App\Http\Controllers;

use App\Models\RoomDet;
use Illuminate\Http\Request;

class RoomDetCRUDController extends CRUDController
{
    const MODEL = RoomDet::class;
    const TITLE_SINGULAR = 'Oda Detayı';
    const TITLE_PLURAL = 'Oda Detayları';


    protected $prefix = 'room_det';

    protected function getColumns() {
        return [
            [
                'name' => 'room',
                'label' => 'Oda',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'beds',
                'label' => 'Yatak Sayısı',
                'type' => 'number',
                'id'=> 'kt_inputmask_10',
                'colSize' => 6,
            ],
            [
                'name' => 'cabinets',
                'label' => 'Dolap Sayısı',
                'type' => 'number',
                'id'=> 'kt_inputmask_10',
                'colSize' => 6,
            ],
            [
                'name' => 'refrigerator',
                'label' => 'Buzdolabı Sayısı',
                'type' => 'number',
                'id'=> 'kt_inputmask_10',
                'colSize' => 6,
            ],
            [
                'name' => 'klima',
                'label' => 'Klima',
                'type' => 'number',
                'id'=> 'kt_inputmask_10',
                'colSize' => 6,
            ],
            [
                'name' => 'washing_machine',
                'label' => 'Çamaşır Makinası',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'television',
                'label' => 'Televizyon',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'kitchen',
                'label' => 'Durum',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'bathroom',
                'label' => 'Tuvalet/Banyo',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'price',
                'label' => 'Kira Ücreti',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
        ];
    }   
}
