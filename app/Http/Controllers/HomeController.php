<?php

namespace App\Http\Controllers;


use App\Models\Tenant;
use App\Models\Floor;
use App\Models\Room;
use App\Models\Revenue;
use App\Models\Expense;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

      
   

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function index(Request $request)
    {
  
        $tenantscount = Tenant::All('id')->count();
        $floorscount = Floor::All('id')->count();
        $roomscount = Room::All('id')->count();
        $roomsum = Room::All('roomCount')->sum('roomCount');
        $revenuesum = Revenue::All('price')->sum('price');
        $expensesum = Expense::All('price')->sum('price');
        $lastTenants = Tenant::orderBy('id','desc')->take(8)->get();
        return view('home', [
            'tenantscount' => $tenantscount,
            'floorscount' => $floorscount,
            'roomscount' => $roomscount,
            'roomsum' => $roomsum,
            'revenuesum' => $revenuesum,
            'expensesum' => $expensesum,
            'lastTenants' => $lastTenants,
        ]);

        $request->user()->authorizeRoles([
            'manager',
            'admin',
            'finans',
            'user'
        ]);

        return view('home');
    }

    // public function someAdminStuff(Request $request){

    //     $request->user()->authorizeRoles('admin');
    //     return view('home');
    // }
}
