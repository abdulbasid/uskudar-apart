<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index() {

        $contacts = Contact::All();
        return view('front.contact', [
            'contacts' => $contacts,
        ]);
        
        return view('front.contact');
    }

}
