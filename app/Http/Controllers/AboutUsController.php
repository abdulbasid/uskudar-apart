<?php

namespace App\Http\Controllers;

use App\Models\Aboutus;
use Illuminate\Http\Request;

class AboutUsController extends Controller
{   

    public function index(Request $request)
    {

        $aboutuses = Aboutus::All();
        return view('front.about-us', [
            'aboutuses' => $aboutuses,
        ]);

        return view('front.about-us');
    }
}
