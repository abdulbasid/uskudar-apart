<?php

namespace App\Http\Controllers;

use App\Models\Expense;
use Illuminate\Http\Request;
use App\Models\ExpenseList;

class ExpenseCRUDController extends CRUDController
{
    const MODEL = Expense::class;
    const TITLE_SINGULAR = 'Gider';
    const TITLE_PLURAL = 'Giderler';


    protected $prefix = 'expenses';

    protected function getColumns() {
        return [
            [
                'name' => 'expense_list_id',
                'label' => 'Gider Türü',
                'type' => 'join-1n',
                'relation' => 'reference',
                'relation_key' => 'fullName',
                'model' => ExpenseList::class,
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'price',
                'label' => 'Ücret',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
        ];
    }
}
