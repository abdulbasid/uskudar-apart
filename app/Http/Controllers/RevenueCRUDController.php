<?php

namespace App\Http\Controllers;

use App\Models\Revenue;
use App\Models\Tenant;
use Illuminate\Http\Request;


class RevenueCRUDController extends CRUDController
{
    const MODEL = Revenue::class;
    const TITLE_SINGULAR = 'Gelir';
    const TITLE_PLURAL = 'Gelirler';


    protected $prefix = 'revenues';

    protected function getColumns() {
        return [
            [
                'name' => 'tenant_id',
                'label' => 'Kiracı Adı',
                'type' => 'join-1n',
                'relation' => 'reference',
                'relation_key' => 'fullName',
                'model' => Tenant::class,
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'price',
                'label' => 'Ücret',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'date',
                'label' => 'Tarih',
                'type' => 'date',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'note',
                'label' => 'Not',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
        ];
    }
}
