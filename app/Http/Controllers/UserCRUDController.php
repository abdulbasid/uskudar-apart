<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserCRUDController extends CRUDController
{
    const MODEL = User::class;
    const TITLE_SINGULAR = 'Kullanıcı';
    const TITLE_PLURAL = 'Kullanıcılar';


    protected $prefix = 'users';

    protected function getColumns() {
        return [
            [
                'name' => 'name',
                'label' => 'Adı',
                'type' => 'text',
                'id'=> '',
                'colSize' => 6,
            ],
            [
                'name' => 'email',
                'label' => 'E-Mail',
                'type' => 'text',
                'id'=> 'kt_inputmask_9',
                'colSize' => 6,
            ],
            [
                'name' => 'password',
                'label' => 'Şifre',
                'type' => 'password',
                'id'=> '',
                'colSize' => 6,
            ],
        ];
    }
}
