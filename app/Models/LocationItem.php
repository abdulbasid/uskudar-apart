<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LocationItem extends Model
{
    protected $fillable = [
        'item'
    ];
}
