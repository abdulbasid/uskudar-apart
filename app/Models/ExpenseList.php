<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExpenseList extends Model
{
    protected $fillable = [
        'name'
    ];

    /**
     * Get the tenat's full name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return "{$this->name}";
    }
}
