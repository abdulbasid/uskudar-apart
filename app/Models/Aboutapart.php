<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aboutapart extends Model
{
    protected $fillable = [
        'title',
        'paragraph',
        'paragraph1',
        'paragraph2'
    ];
}
