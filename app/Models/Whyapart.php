<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Whyapart extends Model
{
    protected $fillable = [
        'title',
        'paragraph',
        'photo'
    ];
}
