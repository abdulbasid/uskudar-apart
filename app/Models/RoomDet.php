<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoomDet extends Model
{
    protected $fillable = [ 
        'room',
        'beds',
        'cabinets',
        'refrigerator',
        'klima',
        'washing_machine',
        'television',
        'kitchen',
        'bathroom',
        'price'
    ];
}
