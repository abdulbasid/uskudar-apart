<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PhotoGalery extends Model
{
    protected $fillable = [
        'photo',
        'category'
    ];
}
