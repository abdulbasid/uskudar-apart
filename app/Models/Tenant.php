<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{
    protected $fillable = [
        'tc',
        'name',
        'surname',
        'phone',
        'email',
        'job',
        'price',
        'startdate',
        'enddate',
        'floor_id',
        'room_id',
        'statu'
    ];

    public function reference()
    {
        return $this->belongsTo('App\Models\Floor', 'floor_id');
    }

    public function references()
    {
        return $this->hasMany('App\Models\Floor', 'floor_id');
    }

    public function room()
    {
        return $this->belongsTo('App\Models\Room', 'room_id');
    }
    
    public function rooms()
    {
        return $this->hasMany('App\Models\Room', 'room_id');
    }


    /**
     * Get the tenat's full name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return "{$this->name} {$this->surname}";
    }

    public function getFloorAttribute()
    {
        return "{$this->name}";
    }
    
    public function getRoomNumberAttribute()
    {
        return "{$this->number}";
    }
}
