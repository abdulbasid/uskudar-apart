<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $fillable = [
        'expense_list_id',
        'price'
    ];

    public function reference()
    {
        return $this->belongsTo('App\Models\ExpenseList', 'expense_list_id');
    }

    public function references()
    {
        return $this->hasMany('App\Models\ExpenseList', 'expense_list_id');
    }

    /**
     * Get the tenat's full name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return "{$this->name}";
    }
}
