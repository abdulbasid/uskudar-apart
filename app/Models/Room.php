<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = [
        'floor_id',
        'roomCount',
        'number',
        'remaining_number'
    ];
    
    
    public function reference()
    {
        return $this->belongsTo('App\Models\Floor', 'floor_id');
    }

    public function references()
    {
        return $this->hasMany('App\Models\Floor', 'floor_id');
    }

    
    /**
     * Get the tenat's full name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return "{$this->floor} ";
    }

    public function getRoomNumberAttribute()
    {
        return "{$this->number}";
    }

}
