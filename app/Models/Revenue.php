<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Revenue extends Model
{
    protected $fillable = [
        'tenant_id',
        'price',
        'date',
        'note'
    ];

    public function reference()
    {
        return $this->belongsTo('App\Models\Tenant', 'tenant_id');
    }

    public function references()
    {
        return $this->hasMany('App\Models\Tenant', 'tenant_id');
    }

    /**
     * Get the tenat's full name.
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        return "{$this->name} {$this->surname}";
    }
}
