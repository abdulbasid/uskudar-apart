<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApartProperty extends Model
{
    protected $fillable = [
        'order',
        'property'
    ];
}
