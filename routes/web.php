<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes([
    // 'register'=> false,
]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/welcome', 'MainPageController@index')->name('welcome');
Route::get('/', 'MainPageController@index')->name('welcome');
Route::name('front.')->group(function () {
    Route::get('about-us', 'AboutUsController@index')->name('about-us');
    Route::get('about-apart', 'AboutApartController@index')->name('about-apart');
    Route::get('galery', 'GaleryController@index')->name('galery');
    Route::get('contact', 'ContactController@index')->name('contact');
});

Route::name('errors.')->group(function () {
    Route::get('/404', 'HomeController@index')->name('404');
    Route::get('/500', 'HomeController@index')->name('500');
});


Route::get('/revenue-report', 'ReportRevenueController@index')->name('report.revenue');

Auth::routes();

/* --- CRUD Controllers - START --- */
Route::resource('users', UserCRUDController::class);  //Kullanıcılar
Route::resource('tenants', TenantCRUDController::class);  //Kiracı
Route::resource('revenues', RevenueCRUDController::class);  //Gelirler
Route::resource('expenses', ExpenseCRUDController::class);  //Giderler
Route::resource('expense_lists', ExpenseListCRUDController::class);  //Gider Türleri
Route::resource('room_det', RoomDetCRUDController::class);  //Odalar
Route::resource('aboutus', AboutusCRUDContoller::class);  //Hakkımızda
Route::resource('aboutapart', AboutapartCRUDController::class);  //Apart Hakkında
Route::resource('contacts', ContactCRUDController::class);  //İletişim
Route::resource('rooms', RoomCRUDController::class);  //Odalar
Route::resource('floors', FloorCRUDController::class);  //Katlar


Route::resource('whyapart', WhyapartCRUDController::class);  //Neden Üsküdar Apart
Route::resource('locationitem', LocationItemCRUDController::class);  //Konum Maddeleri
Route::resource('apartproperty', ApartPropertyCRUDController::class);  //Apart Özellikleri
Route::resource('photogalery', PhotoGaleryCRUDController::class);  //Fotoğraf Galerisi



/* --- CRUD Controllers - END --- */

Route::get('/user', 'UserController@index')->name('admin.users.list');

Route::namespace('Auth')->group(function () {
    Route::get('logout', 'LoginController@logout');
});
