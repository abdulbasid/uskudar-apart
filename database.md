# User (Employee)
- id
- role_id
...

# Source
- id
- name
- slug
- code

# Doctor
- id
- name
- surname
- title
- profession

# CustomerStatus
- id
- slug
- code
- name
- color

# Customer
- id
- reference_id (Customer, nullable)
- source_id (Source)
- doctor_id (Doctor)
- status_id (CustomerStatus)
- agent_id (User, nullable)
- name
- surname
- gsm
- email
- boy (double)
- kilo (double)
- yas
- dogum sayisi (int)
- ulke (Country)
- sehir (string)
- yaptigi is
- hastalik
- kullandigi ilaclar
- onceki operasyonlar
- sigara (bool)
- alkol (bool)
- cinsiyet (enum[male, famale])
- not (text)
- durum
- callable_at (timestamp, nullable)

# OperationCategory
- id
- name
- code

# Operation
- id
- category_id (OperationCategory)
- name
- price
- face_related (bool)
- liposuction_required (bool)
- minimum (int)
- maximum (int)
- step (int)

# Offer
- id
- customer_id (Customer)
- doctor_id (Doctor)
- agent_id (User)
- offered_at

# OfferOperation [CRUD=false]
- id
- offer_id
- operation_id
- quantity

# Campaign
- id
- name
- type (enum[same-process, different-process])
- started_at
- finished_at (Nullable)

# OfferCampaign [CRUD=false]
- id
- offer_id
- campaign_id
