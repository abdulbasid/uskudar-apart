@extends('layouts.inside')
@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="row">
        <div class="col-lg-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">Güncelle <small> {{$pluralTitle}}</small></h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <a href="{{ url()->previous() }}" class="btn btn-clean kt-margin-r-10">
                            <i class="la la-arrow-left"></i>
                            <span class="kt-hidden-mobile">Geri</span>
                        </a>
                        <form class="kt-form kt-form--label-right" method="POST" action="{{ route($updateRoute, $row) }}">
                            @csrf
                            @method('PUT')
                            <div class="btn-group">
                                <button type="submit" class="btn btn-brand">
                                    <i class="la la-check"></i>
                                    <span class="kt-hidden-mobile">Güncelle</span>
                                </button>
                                <button type="button" class="btn btn-brand dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                </button>
                                <div class="dropdown-menu dropdown-menu-right">
                                    <ul class="kt-nav">
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-reload"></i>
                                                <span class="kt-nav__link-text">Save & continue</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-power"></i>
                                                <span class="kt-nav__link-text">Save & exit</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-edit-interface-symbol-of-pencil-tool"></i>
                                                <span class="kt-nav__link-text">Save & edit</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__item">
                                            <a href="#" class="kt-nav__link">
                                                <i class="kt-nav__link-icon flaticon2-add-1"></i>
                                                <span class="kt-nav__link-text">Save & add new</span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <!--begin::Form-->
                <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
                    <div class="row">
                        <div class="col-lg-12">

                            <!--begin::Portlet-->
                            <div class="kt-portlet">

                                <!--begin::Form-->
                                <form class="kt-form kt-form--label-right" method="POST" action="{{ route($updateRoute, $row) }}">
                                    @csrf
                                    @method('PUT')
                                    <div class="kt-portlet__body">
                                        <div class="form-group row">
                                            @foreach($columns as $column)
                                            <div class="col-lg-{{ $column['colSize'] ? $column['colSize'] : 12 }}">
                                                <label for="{{$column['name']}}">{{$column['label']}}:</label>
                                                @if (isset($column['relation']) && $column['relation'])
                                                <select name="{{$column['name']}}" placeholder="{{$column['label']}}" class="form-control">
                                                    @foreach($column['model']::all() as $option)
                                                    <option value="{{$option->id}}" {{$option->id == $row->{$column['name']} ? 'selected' : ''}}>{{ $option->{$column['relation_key']} }}</option>
                                                    @endforeach
                                                </select>
                                                @else
                                                <input type="text" class="form-control" name="{{$column['name']}}" placeholder="{{$column['label']}}" value="{{$row[$column['name']]}}">
                                                @endif
                                                @if(isset($column['description']) && !empty($column['description']))
                                                <span class="form-text text-muted">{{ $column['description'] }}</span>
                                                @endif
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="kt-portlet__foot">
                                        <div class="kt-form__actions">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <button type="submit" class="btn btn-primary">Güncelle</button>
                                                    <button type="reset" class="btn btn-secondary">Temizle</button>
                                                </div>
                                                <div class="col-lg-6 kt-align-right">
                                                    <button type="reset" class="btn btn-danger">Sil</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                                <!--end::Form-->
                            </div>
                            <!--end::Portlet-->
                        </div>
                    </div>
                </div>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
@endsection