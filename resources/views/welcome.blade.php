@extends('layouts.outside')

@section('content')
    <!-- Main Theme Wrapper -->
    <div id="Wrapper">
        <!-- Header Wrapper -->
        <div id="Header_wrapper">
            <!-- Header -->
            <header id="Header">
                <!-- Revolution slider area-->
                <div class="mfn-main-slider" id="mfn-rev-slider">
                    <div id="rev_slider_52_2_wrapper" class="rev_slider_wrapper fullscreen-container" style="padding:0px;">
                        <div id="rev_slider_52_2" class="rev_slider fullscreenbanner">
                            <ul>
                                <li data-transition="fade" data-slotamount="7" data-masterspeed="300" data-saveperformance="off">
                                    <img src="{{ asset('images/home_developer_slider.jpg') }}" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                                    <div class="tp-caption tp-fade" data-x="center" data-hoffset="0" data-y="100" data-speed="1000" data-start="500" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 5;"><img src="{{ asset('images/home_developer_slider_logo.png') }}" alt="">
                                    </div>
                                    <!-- <div class="tp-caption sfb" data-x="center" data-hoffset="0" data-y="center" data-voffset="0" data-speed="800" data-start="1500" data-easing="easeInOutCubic" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 6;"><img src="{{ asset('images/home_developer_slider_text.png') }}" alt="">
                                    </div> -->
                                    <div class="tp-caption sfb" data-x="center" data-hoffset="-170" data-y="bottom" data-voffset="60" data-speed="1000" data-start="2300" data-easing="Back.easeOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 7;">
                                        <a href="#video"><img src="{{ asset('images/home_developer_slider_link_1.png') }}" alt="">
                                        </a>
                                    </div>
                                    <div class="tp-caption sfb" data-x="center" data-hoffset="0" data-y="bottom" data-voffset="60" data-speed="1000" data-start="2900" data-easing="Back.easeOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 8;">
                                        <a href="#location"><img src="{{ asset('images/home_developer_slider_link_2.png') }}" alt="">
                                        </a>
                                    </div>
                                    <div class="tp-caption sfb" data-x="center" data-hoffset="170" data-y="bottom" data-voffset="56" data-speed="1000" data-start="3500" data-easing="Back.easeOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 9;">
                                        <a href="{{ route('front.galery')}}"><img src="{{ asset('images/home_developer_slider_link_3.png') }}" alt="">
                                        </a>
                                    </div>
                                </li>
                                <li data-transition="fade" data-slotamount="7" data-masterspeed="300" data-saveperformance="off">
                                    <img src="{{ asset('images/home_developer_slider.jpg') }}" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                                    <div class="tp-caption tp-fade" data-x="center" data-hoffset="0" data-y="100" data-speed="1000" data-start="500" data-easing="Power3.easeInOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 5;"><img src="{{ asset('images/home_developer_slider_logo.png') }}" alt="">
                                    </div>
                                    <div class="tp-caption sfb" data-x="center" data-hoffset="0" data-y="center" data-voffset="0" data-speed="800" data-start="1500" data-easing="easeInOutCubic" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 6;"><img src="{{ asset('images/home_developer_slider_text.png') }}" alt="">
                                    </div>
                                    <div class="tp-caption sfb" data-x="center" data-hoffset="-170" data-y="bottom" data-voffset="60" data-speed="1000" data-start="2300" data-easing="Back.easeOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 7;">
                                        <a href="#"><img src="{{ asset('images/home_developer_slider_link_1.png') }}" alt="">
                                        </a>
                                    </div>
                                    <div class="tp-caption sfb" data-x="center" data-hoffset="0" data-y="bottom" data-voffset="60" data-speed="1000" data-start="2900" data-easing="Back.easeOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 8;">
                                        <a href="#"><img src="{{ asset('images/home_developer_slider_link_2.png') }}" alt="">
                                        </a>
                                    </div>
                                    <div class="tp-caption sfb" data-x="center" data-hoffset="170" data-y="bottom" data-voffset="56" data-speed="1000" data-start="3500" data-easing="Back.easeOut" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" style="z-index: 9;">
                                        <a href="#"><img src="{{ asset('images/home_developer_slider_link_3.png') }}" alt="">
                                        </a>
                                    </div>
                                </li>
                            </ul>
                            <div class="tp-bannertimer tp-bottom flv_viz_hid"></div>
                        </div>
                    </div>

                </div>
            </header>
        </div>
        <!-- Main Content -->
        <div id="Content">
            <div class="content_wrapper clearfix">
                <div class="sections_group">
                    <div class="entry-content">
                        <div class="section" style="padding-top:70px; padding-bottom:20px; background-color:#fff">
                            <div class="section_wrapper clearfix">
                                <div class="items_group clearfix">
                                    <!-- Page Title-->
                                    <!-- One full width row-->
                                    <div class="column one column_fancy_heading">
                                        <div class="fancy_heading fancy_heading_icon">
                                            <h2 class="title">Neden Üsküdar Apart?</h2>
                                        </div>
                                    </div>
                                    <!-- One full width row-->
                                    <div class="column one column_offer_thumb">
                                        <div class="offer_thumb">
                                            <ul class="offer_thumb_ul">
                                            @foreach($whyaparts as $whyapart)
                                                <li class="offer_thumb_li">
                                                    <div class="image_wrapper"><img width="585" height="410" src="{{ asset('images/home_developer_thumb_slider_1.jpg') }}" class="scale-with-grid wp-post-image" alt="home_developer_thumb_slider_1" />
                                                    </div>
                                                    <div class="desc_wrapper">
                                                        <div class="desc">
                                                            <h4 style="margin-top: 18px;">{{$whyapart->title}}</h4>
                                                            <p class="big">
                                                                {{$whyapart->paragraph}}
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="thumbnail" style="display:none"><img src="{{ asset('images/home_developer_thumb_slider_icon_1.png') }}" class="scale-with-grid" alt="">
                                                    </div>
                                                </li>
                                            @endforeach
                                            </ul>
                                            <div class="slider_pagination"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a name="location"></a>
                        <div class="section" style="padding-top:80px; padding-bottom:40px; background-color:#f3f3f1">
                            <div class="section_wrapper clearfix">
                                <div class="items_group clearfix">
                                    <!-- Three Fourth (3/4) Column -->
                                    <div class="column three-fourth column_map">
                                   
                                        <!-- Google map area -->
                                        <div class="google-map-wrapper">
                                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d376.2543259229571!2d29.017298!3d41.0244988!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14cab78be5dbaa03%3A0xc21b162757bb35b3!2sBosch!5e0!3m2!1str!2str!4v1563795477142!5m2!1str!2str" width="100%" height="410" frameborder="0" style="border:0"></iframe>
                                        </div>
                                    </div>
                                    <!-- One Fourth (1/4) Column -->
                                    <div class="column one-fourth column_column">
                                        <div class="column_attr animate" data-anim-type="fadeInRight">
                                            <h2 style="margin-top: 10px;">Üsküdar da en iyi
												<br>
												konuma sahip apart
												<br>
												"Üsküdar Apart"</h2>
                                            <p style="margin-bottom: 25px;">
                                               
                                            </p>
                                            <ul class="list_idea">
                                            @foreach($locationitems as $locationitem) 
                                                <li>
                                                {{$locationitem->item}}
                                                </li>
                                            @endforeach
                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="section sections_style_3">
                            <div class="section_wrapper clearfix">
                                <div class="items_group clearfix">
                                    <!-- One Third (1/3) Column -->
                                    @foreach($apartproperties as $apartproperty) 
                                    <div class="column one-third column_list">
                                        <div class="list_item lists_4 clearfix">
                                            <!-- Animated area -->
                                            <div class="animate" data-anim-type="fadeInUp">
                                                <div class="circle">
                                                {{$apartproperty->order}}
                                                </div>
                                                <div class="list_right">
                                                    <div class="desc">
                                                     {{$apartproperty->property}}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="section" style="padding-top:0px; padding-bottom:20px; background-image:url({{ asset('images/home_developer_section_gallery.jpg') }}); background-repeat:no-repeat; background-position:center; ">
                            <div class="section_wrapper clearfix">
                                <div class="items_group clearfix">
                                    <!-- Page devider -->
                                    <!-- One full width row-->
                                    <div class="column one column_divider">
                                        <div class="hr_wide " style="margin: 0 auto 50px;">
                                            <hr>
                                        </div>
                                    </div>
                                    <!-- Page Title-->
                                    <!-- One full width row-->
                                    <div class="column one column_fancy_heading">
                                        <div class="fancy_heading fancy_heading_icon">
                                            <span class="icon_top"><i class="far fa-images"></i></span>
                                            <h2 class="title">Fotoğraf Galerisi</h2>
                                            <div class="inside">
                                                <span class="big">1, 2, 3, 4 kişilik eşyalı paylaşımlı odalar ile konumuyla tüm ulaşım.
														<br>
														araçlarına yakın olmasıyla ilgii çeken bir Apart.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- One full width row-->
                                    <div class="column one column_slider">
                                        <!-- Slider Content -->
                                        <div class="content_slider ">
                                            <ul class="content_slider_ul">
                                                <li class="content_slider_li_1"><img width="890" height="470" src="{{ asset('images/home_developer_photo_slider_1-890x470.jpg') }}" class="scale-with-grid wp-post-image" alt="home_developer_photo_slider_1" />
                                                </li>
                                                <li class="content_slider_li_2"><img width="890" height="470" src="{{ asset('images/home_developer_photo_slider_3-890x470.jpg') }}" class="scale-with-grid wp-post-image" alt="home_developer_photo_slider_3" />
                                                </li>
                                                <li class="content_slider_li_3"><img width="890" height="470" src="{{ asset('images/home_developer_photo_slider_2-890x470.jpg') }}" class="scale-with-grid wp-post-image" alt="home_developer_photo_slider_2" />
                                                </li>
                                                <li class="content_slider_li_4"><img width="890" height="470" src="{{ asset('images/home_developer_photo_slider_5-890x470.jpg') }}" class="scale-with-grid wp-post-image" alt="home_developer_photo_slider_5" />
                                                </li>
                                                <li class="content_slider_li_5"><img width="890" height="470" src="{{ asset('images/home_developer_photo_slider_4-890x470.jpg') }}" class="scale-with-grid wp-post-image" alt="home_developer_photo_slider_4" />
                                                </li>
                                            </ul>
                                            <a class="button button_js slider_prev" href="#">
                                                <span class="button_icon">
                                                    <i class="fas fa-chevron-left"></i>
                                                </span>
                                            </a>
                                            <a class="button button_js slider_next" href="#">
                                                <span class="button_icon">
                                                    <i class="fas fa-chevron-right"></i>
                                                </span>
                                            </a>
                                            <div class="slider_pagination"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="section" style="padding-top:0px; padding-bottom:20px; ">
                            <div class="section_wrapper clearfix">
                                <div class="items_group clearfix">
                                    <!-- Page devider -->
                                    <!-- One full width row-->
                                    <div class="column one column_divider">
                                        <div class="hr_wide hrmargin_b_60">
                                            <hr>
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                        <a name="video"></a>
                        <div class="section dark " style="padding-top:20px; padding-bottom:20px; background-color:#ffba00">
                            <div class="section_wrapper clearfix">
                                <div class="items_group clearfix">
                                    <!-- One full width row-->
                                    <div class="column one column_call_to_action">
                                        <div class="call_to_action">
                                            <!-- Animated area -->
                                            <div class="animate" data-anim-type="fadeIn">
                                                <div class="call_to_action_wrapper">
                                                    <div class="call_left">
                                                   
                                                        <h3>Üsküdar Apart Tanıtım Videosu</h3>
                                                    </div>
                                                    <div class="call_center">
                                                        <a href="https://www.youtube.com/watch?v=3bWaJsOzRK4?autoplay=0" rel="prettyphoto"><span class="icon_wrapper"><i class="fas fa-play"></i></span></a>
                                                    </div>
                                                    <div class="call_right">
                                                        <div class="desc">
                                                            Curabitur et ligula. Ut molestie a, ultricies porta urna. Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="section dark " style="padding-top:150px; padding-bottom:110px; background-image:url({{ asset('images/home_developer_counters.jpg') }}); background-repeat:no-repeat; background-position:center top; background-attachment:fixed; background-size:cover; -webkit-background-size:cover" data-stellar-background-ratio="0.5">
                            <div class="section_wrapper clearfix">
                                <div class="items_group clearfix">
                                    <!-- Page Title-->
                                    <!-- One full width row-->
                                    <div class="column one column_fancy_heading">
                                        <div class="fancy_heading fancy_heading_icon">
                                            <!-- Animated area -->
                                            <div class="animate" data-anim-type="zoomIn">
                                                <h2 class="title">Apart Canlı Form</h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="column one-third column_quick_fact">
                                        <!-- Counter area-->
                                        <div class="quick_fact animate-math">
                                            <!-- Animated area -->
                                            <div class="animate" data-anim-type="bounceIn">
                                                <div class="number" data-to="{{$floorscount}}">
                                                    0
                                                </div>
                                                <h3 class="title">Kat Sayısı</h3>
                                                <hr class="hr_narrow" />
                                                <div class="desc">
                                                   Apartta bulunan toplam
                                                    <br> kat sayısı {{$floorscount}}'tür.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- One Third (1/3) Column -->
                                    <div class="column one-third column_quick_fact">
                                        <!-- Counter area-->
                                        <div class="quick_fact animate-math">
                                            <!-- Animated area -->
                                            <div class="animate" data-anim-type="bounceIn">
                                                <div class="number" data-to="{{$roomscount}}">
                                                    0
                                                </div>
                                                <h3 class="title">Oda Sayısı</h3>
                                                <hr class="hr_narrow" />
                                                <div class="desc">
                                                   Apartta bulunan toplam
                                                    <br> oda sayısını vermektedir. 1, 2, 3, 4 kişilik odalar.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- One Third (1/3) Column -->
                                    <div class="column one-third column_quick_fact">
                                        <!-- Counter area-->
                                        <div class="quick_fact animate-math">
                                            <!-- Animated area -->
                                            <div class="animate" data-anim-type="bounceIn">
                                                <div class="number" data-to="{{$tenantscount}}">
                                                    0
                                                </div>
                                                <h3 class="title">Kiracı Sayısı</h3>
                                                <hr class="hr_narrow" />
                                                <div class="desc">
                                                   Şu anda kalan toplam kişi
                                                    <br> sayısını vermektedir.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
 
    </div>
   <!-- Popup contact form -->
    <div id="popup_contact">
        <a class="button button_js" href="#"><i class="far fa-envelope"></i></a>
        <div class="popup_contact_wrapper">

            <div id="contactWrapper_popup">
                <!-- Contact form area-->
                <form id="contactform_popup">
                    <h4>Mesaj Gönder</h4>
                    <p>
                        <span>
								<input type="text" name="name_popup" id="name_popup" size="40" aria-required="true" aria-invalid="false" placeholder="Ad" />
                            </span>
                            <span>
								<input type="email" name="email_popup" id="email_popup" size="40" aria-required="true" aria-invalid="false" placeholder="E-Mail" />
                            </span>
                            <span>
								<input type="text" name="tel_popup" id="tel_popup" size="40" aria-required="true" aria-invalid="false" placeholder="Telefon" />
                            </span>
                            <span>
								<input type="text" name="subject_popup" id="subject_popup" size="40" aria-required="true" aria-invalid="false" placeholder="Konu" />
                            </span>
                            <span> 								
                                <textarea name="body_popup" cols="40" id="body_popup" rows="2" aria-required="true" aria-invalid="false" placeholder="Mesaj">
                                </textarea>
                            </span>

                        <input type="button" value="Mesaj Gönder" id="submit_popup" onClick="return check_values_popup();">
                    </p>
                </form>
                <div id="confirmation_popup"></div>
            </div>

            <span class="arrow"></span>
        </div>
    </div>
    


</html>
@endsection