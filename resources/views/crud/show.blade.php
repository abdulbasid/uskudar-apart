@extends('layouts.inside')
@section('content')

<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-subheader__main">
        <h3 class="kt-subheader__title">
        <a href="{{ route('home')}}" class="kt-subheader__breadcrumbs-link">
            Panel 
        </a>
        </h3>
        <span class="kt-subheader__separator kt-hidden">
        </span>
        
        <div class="kt-subheader__breadcrumbs">
            <span class="kt-subheader__breadcrumbs-separator">
            </span>
            <a href="" class="kt-subheader__breadcrumbs-link">
            {{$pluralTitle}}
            </a>
            <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
        </div>
    </div>
    <div class="kt-subheader__toolbar">
        <div class="kt-subheader__wrapper">
            <a href="#" class="btn kt-subheader__btn-daterange" id="kt_dashboard_daterangepicker" data-toggle="kt-tooltip" title="" data-placement="left" data-original-title="Select dashboard daterange">
                <span class="kt-subheader__btn-daterange-title" id="kt_dashboard_daterangepicker_title">Today</span>&nbsp;
                <span class="kt-subheader__btn-daterange-date" id="kt_dashboard_daterangepicker_date">Aug 16</span>

                <!--<i class="flaticon2-calendar-1">
                </i>-->
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--sm">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect id="bound" x="0" y="0" width="24" height="24">
                        </rect>
                        <path d="M4.875,20.75 C4.63541667,20.75 4.39583333,20.6541667 4.20416667,20.4625 L2.2875,18.5458333 C1.90416667,18.1625 1.90416667,17.5875 2.2875,17.2041667 C2.67083333,16.8208333 3.29375,16.8208333 3.62916667,17.2041667 L4.875,18.45 L8.0375,15.2875 C8.42083333,14.9041667 8.99583333,14.9041667 9.37916667,15.2875 C9.7625,15.6708333 9.7625,16.2458333 9.37916667,16.6291667 L5.54583333,20.4625 C5.35416667,20.6541667 5.11458333,20.75 4.875,20.75 Z" id="check" fill="#000000" fill-rule="nonzero" opacity="0.3">
                        </path>
                        <path d="M2,11.8650466 L2,6 C2,4.34314575 3.34314575,3 5,3 L19,3 C20.6568542,3 22,4.34314575 22,6 L22,15 C22,15.0032706 21.9999948,15.0065399 21.9999843,15.009808 L22.0249378,15 L22.0249378,19.5857864 C22.0249378,20.1380712 21.5772226,20.5857864 21.0249378,20.5857864 C20.7597213,20.5857864 20.5053674,20.4804296 20.317831,20.2928932 L18.0249378,18 L12.9835977,18 C12.7263047,14.0909841 9.47412135,11 5.5,11 C4.23590829,11 3.04485894,11.3127315 2,11.8650466 Z M6,7 C5.44771525,7 5,7.44771525 5,8 C5,8.55228475 5.44771525,9 6,9 L15,9 C15.5522847,9 16,8.55228475 16,8 C16,7.44771525 15.5522847,7 15,7 L6,7 Z" id="Combined-Shape" fill="#000000">
                        </path>
                    </g>
                </svg> </a>
            
        </div>
    </div>
</div>

@if (\Request::is('revenues/*'))

<div class="kt-content  kt-grid__item kt-grid__item--fluid"  id="kt_content">
    <div class="row">
        <div class="col-lg-12">
            <div class="kt-portlet">
                <div id="print-head" class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">Görüntüle <small> {{$pluralTitle}}</small></h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <a href="{{ url()->previous() }}" class="btn btn-clean kt-margin-r-10">
                            <i class="la la-arrow-left"></i>
                            <span class="kt-hidden-mobile">Geri</span>
                        </a>
                        <div class="btn-group">
                            <button type="button" class="btn btn-brand">
                                <i class="la la-check"></i>
                                <span class="kt-hidden-mobile">Gönder</span>
                            </button>
                          
                            <button type="button" class="btn btn-brand dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            </button>

                            <input type="button" id="print_calculate" class="btn btn-brand"  value="Yazdır"></li>    
                        
                            <div class="dropdown-menu dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon-email"></i>
                                            <span class="kt-nav__link-text">E-Mail Gönder</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon-list"></i>
                                            <span class="kt-nav__link-text">PDF İndir</span>
                                        </a>
                                    </li>
                                   
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
                <div class="row"  id="divYazdir">
                    <div class="col-lg-12">
                        <div class="kt-portlet">
                            <div class="kt-portlet__body kt-portlet__body--fit">
                                <div class="kt-invoice-2">
                                    <div class="kt-invoice__wrapper">
                                        <div class="kt-invoice__head">
                                            <div class="kt-invoice__container kt-invoice__container--centered">
                                                <div class="kt-invoice__logo">
                                                    <a href="">
                                                        <h1>TEŞEKKÜR EDERİZ</h1>
                                                    </a>
                                                   
                                                    <a href="#">
                                                        <img src="{{ asset('images/developer.png') }}">
                                                    </a>
                                                </div>
                                                <span class="kt-invoice__desc">
                                                    <span> Mimar Sinan mah. Selami Ali efendi cad</span>
                                                    <span> Şerifali Apartmanı No : 11 Daire : 3 </span>
                                                    <span> Üsküdar / İstanbul</span>
                                                </span>
                                                <div class="kt-invoice__items">
                                                    <div class="kt-invoice__item">
                                                        <span class="kt-invoice__subtitle">İşlem Yapan</span>
                                                        <span class="kt-invoice__text"> {{ucfirst(auth()->user()->name)}}</span>
                                                    </div>
                                                    <div class="kt-invoice__item">
                                                        <span class="kt-invoice__subtitle">İşlem Tarihi</span>
                                                        <span class="kt-invoice__text">{{date('d.m.Y')}}</span>
                                                    </div>
                                                   
                                                </div>
                                            </div>
                                            
                                        </div>
                                        
                                        <div class="kt-invoice__body kt-invoice__body--centered">
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                        <th>Kiracı Adı</th>
                                                            <th>Açıklama</th>
                                                            <th>Kira Veriliş Tarih</th>
                                                            <th>Ödenen</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($revenues as $revenue)
                                                        <tr>
                                                      
                                                            <td> {{$revenue->name}}</td>
                                                            <td> {{$revenue->note}}</td>
                                                            <td> {{$revenue->date}}</td>
                                                            <td  class="kt-font-danger"> {{ number_format($revenue->price, 2, ',', '.') }} ₺</td>
                                                            <!-- <td class="kt-font-danger">{{$revenue->price}} ₺</td> -->       
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="kt-invoice__footer">
                                            <div class="kt-invoice__table  kt-invoice__table--centered table-responsive">
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th>TOPLAM ÖDENEN</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td class="kt-font-danger">{{number_format($revenue->price, 2, ',', '.')}} ₺</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
              
            </div>

        </div>
    </div>
</div>
@else
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="row">
        <div class="col-lg-12">

       
            <div class="kt-portlet">
            
                <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="kt-portlet">

                                <form class="kt-form kt-form--label-right">
                                   
                                    <div class="kt-portlet__body">
                                        <div class="form-group row">
                                        @foreach($columns as $column)
                                        @if($column['type']!="password")
                                            <div class="col-lg-{{ $column['colSize'] ? $column['colSize'] : 12 }}">
                                                <label for="{{$column['name']}}">{{$column['label']}}:</label>
                                                @if (isset($column['relation']) && $column['relation'])
                                                <select name="{{$column['name']}}" placeholder="{{$column['label']}}" disabled class="form-control">
                                                    @foreach($column['model']::all() as $option)
                                                    <option value="{{$option->id}}" {{$option->id == $row->{$column['name']} ? 'selected' : ''}}>{{ $option->{$column['relation_key']} }}</option>
                                                    @endforeach
                                                </select>
                                                @else
                                                <input type="text" class="form-control" disabled name="{{$column['name']}}" placeholder="{{$column['label']}}" value="{{$row[$column['name']]}}">
                                                @endif
                                                @if(isset($column['description']) && !empty($column['description']))
                                                <span class="form-text text-muted">{{ $column['description'] }}</span>
                                                @endif
                                                @endif
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                  
                                </form>
                               
                            </div>
                            
                        </div>
                    </div>
                </div>

              
            </div>

        </div>
    </div>
</div>



@endif




@endsection