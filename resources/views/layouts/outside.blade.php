<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7 "> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]><html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>

    <!-- Basic Page Needs -->
    <meta charset="utf-8">
    <title>Üsküdar Apart</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Favicons -->
    <link rel="shortcut icon" href="images/favicon.ico">

    <!-- FONTS -->
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,400italic,700'>
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Patua+One:100,300,400,400italic,700'>

    <!-- CSS -->
    <link rel='stylesheet' href="{{ asset('front/global.css') }}">
    <link rel='stylesheet' href="{{ asset('front/css/structure.css') }}">
    <link rel='stylesheet' href="{{ asset('front/css/developer.css') }}">
    <link rel='stylesheet' href="{{ asset('front/css/custom.css') }}">
    <link rel='stylesheet' href="{{ asset('front/fontawesome/css/all.min.css') }}">


    <!-- Revolution Slider -->
    <link rel="stylesheet" href="{{ asset('front/plugins/rs-plugin/css/settings.css') }}">

</head>

<body class="template-slider color-one layout-full-width header-creative sticky-white subheader-title-left">  

        @include('front.header')

        @yield('content')
        
        @include('front.footer')
    
               
    <!-- JS -->
    <script src="{{ asset('front/js/jquery-2.1.4.min.js') }}"></script>

    <script src="{{ asset('front/js/mfn.menu.js') }}"></script>
    <script src="{{ asset('front/js/jquery.plugins.js') }}"></script>
    <script src="{{ asset('front/js/jquery.jplayer.min.js') }}"></script>
    <script src="{{ asset('front/js/animations/animations.js') }}"></script>
	<script src="{{ asset('front/js/email.js') }}"></script>
    <script src="{{ asset('front/js/scripts.js') }}"></script>
	

    <script src="{{ asset('front/plugins/rs-plugin/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('front/plugins/rs-plugin/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ asset('front/plugins/rs-plugin/js/extensions/revolution.extension.video.min.js') }}"></script>
    <script src="{{ asset('front/plugins/rs-plugin/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script src="{{ asset('front/plugins/rs-plugin/js/extensions/revolution.extension.actions.min.js') }}"></script>
    <script src="{{ asset('front/plugins/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script src="{{ asset('front/plugins/rs-plugin/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
    <script src="{{ asset('front/plugins/rs-plugin/js/extensions/revolution.extension.navigation.min.js') }}"></script>
    <script src="{{ asset('front/plugins/rs-plugin/js/extensions/revolution.extension.migration.min.js') }}"></script>
    <script src="{{ asset('front/plugins/rs-plugin/js/extensions/revolution.extension.parallax.min.js') }}"></script>

    <script src="http://maps.google.com/maps/api/js?sensor=false&ver=5.9"></script>

    <script>
        var tpj = jQuery;
        tpj.noConflict();
        var revapi52;
        tpj(document).ready(function() {
            revapi52 = tpj('#rev_slider_52_2').show().revolution({
                dottedOverlay: "none",
                delay: 6000,
                startwidth: 1180,
                startheight: 1000,
                hideThumbs: 200,
                thumbWidth: 200,
                thumbHeight: 80,
                thumbAmount: 1,
                simplifyAll: "off",
                navigationType: "none",
                navigationArrows: "none",
                navigationStyle: "round",
                touchenabled: "on",
                onHoverStop: "on",
                nextSlideOnWindowFocus: "off",
                swipe_threshold: 0.7,
                swipe_min_touches: 1,
                drag_block_vertical: false,
                keyboardNavigation: "off",
                navigationHAlign: "left",
                navigationVAlign: "bottom",
                navigationHOffset: 40,
                navigationVOffset: 40,
                soloArrowLeftHalign: "right",
                soloArrowLeftValign: "bottom",
                soloArrowLeftHOffset: 90,
                soloArrowLeftVOffset: 40,
                soloArrowRightHalign: "right",
                soloArrowRightValign: "bottom",
                soloArrowRightHOffset: 40,
                soloArrowRightVOffset: 40,
                shadow: 0,
                fullWidth: "off",
                fullScreen: "on",
                spinner: "spinner3",
                stopLoop: "off",
                stopAfterLoops: 0,
                stopAtSlide: 1,
                shuffle: "off",
                forceFullWidth: "off",
                fullScreenAlignForce: "off",
                minFullScreenHeight: "",
                hideTimerBar: "on",
                hideThumbsOnMobile: "on",
                hideBulletsOnMobile: "off",
                hideArrowsOnMobile: "off",
                hideThumbsUnderResolution: 768,
                fullScreenOffsetContainer: "",
                fullScreenOffset: "",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                startWithSlide: 0
            });
        });
    </script>
    
    <script>
        function google_maps_54af9a7292355() {
            var latlng = new google.maps.LatLng(-33.8710, 151.2039);
            var myOptions = {
                zoom: 16,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                zoomControl: true,
                mapTypeControl: false,
                streetViewControl: false,
                scrollwheel: false
            };
            var map = new google.maps.Map(document.getElementById("google-map-area-54af9a7292355"), myOptions);
            var marker = new google.maps.Marker({
                position: latlng,
                map: map
            });
        }


        jQuery(document).ready(function($) {
            google_maps_54af9a7292355();
        });
    </script>

    <script>
        jQuery(window).load(function() {
            var retina = window.devicePixelRatio > 1 ? true : false;
            if (retina) {
                var retinaEl = jQuery("#logo img.logo-main");
                var retinaLogoW = retinaEl.width();
                var retinaLogoH = retinaEl.height();
                retinaEl.attr("src", "images/retina-developer.png").width(retinaLogoW).height(retinaLogoH);
                var stickyEl = jQuery("#logo img.logo-sticky");
                var stickyLogoW = stickyEl.width();
                var stickyLogoH = stickyEl.height();
                stickyEl.attr("src", "images/retina-developer.png").width(stickyLogoW).height(stickyLogoH);
            }
        });
    </script>

</body>
</html>
