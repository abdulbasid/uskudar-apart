@extends('layouts.login')

@section('content')



<div class="kt-grid kt-grid--ver kt-grid--root">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v3 kt-login--signin" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor"
            style="background-image: url({{ asset('media/bg/bg-3.jpg')}});">
            <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                <div class="kt-login__container">
                    <div class="kt-login__logo">
                        <a href="#">
                            <img src="{{ asset('media/logos/logo-5.png')}}">
                        </a>
                    </div>
                    <div class="kt-login__signin">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title">{{ __('Giriş') }}</h3>
                        </div>
                            @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                            @endif
                        <form method="POST" class="kt-form" action="{{ route('login') }}">
                            @csrf
                            <div class="input-group">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" required autocomplete="email" autofocus
                                    placeholder="{{ __('E-Mail Adresi') }}">

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="input-group">
                                <input id="password" type="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="current-password" placeholder="{{ __('Şifre') }}">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="row kt-login__extra">
                                <div class="col">
                                    <label class="kt-checkbox">
                                        <input type="checkbox" name="remember" id="remember"
                                            {{old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('Beni Hatırla') }}
                                        </label>

                                        <span></span>
                                    </label>
                                </div>
                                <div class="col kt-align-right">
                                    @if (Route::has('password.request'))
                                    <a id="kt_login_forgot" class="kt-login__link" href="{{ route('password.request') }}">
                                        {{ __('Şifremi Unuttum') }}
                                    </a>
                                    @endif
                                </div>
                            </div>
                            <div class="kt-login__actions">
                                <button type="submit" class="btn btn-brand btn-elevate kt-login__btn-primary">
                                    {{ __('Giriş Yap') }}
                                </button>

                            </div>
                        </form>
                    </div>
                    <div class="kt-login__forgot">
                        <div class="kt-login__head">
                            <h3 class="kt-login__title">{{ __('Şifre Yenile') }}</h3>
                            <div class="kt-login__desc">Kayıtlı e-mail adresini giriniz:</div>
                        </div>
                        <form method="POST"  class="kt-form" action="{{ route('password.email') }}">
                        @csrf
                            <div class="input-group">
                                <input id="email" class="form-control @error('email') is-invalid @enderror" type="text" placeholder="{{ __('E-Mail Adresi') }}" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>


                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="kt-login__actions">
                                <button type="submit"
                                    class="btn btn-brand btn-elevate kt-login__btn-primary"> {{ __('Şifre yenileme linki gönder') }}</button>&nbsp;&nbsp;
                                <button id="kt_login_forgot_cancel"
                                    class="btn btn-light btn-elevate kt-login__btn-secondary">Geri</button>
                            </div>
                        </form>
                    </div>                
                </div>
            </div>
        </div>
    </div>
</div>




@endsection