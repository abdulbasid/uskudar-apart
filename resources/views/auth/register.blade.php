@extends('layouts.login')

@section('content')
<div class="kt-grid kt-grid--ver kt-grid--root">
			<div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v3 kt-login--signin" id="kt_login">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-image: url({{ asset('media/bg/bg-3.jpg')}});">
					<div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
						<div class="kt-login__container">
							<div class="kt-login__logo">
								<a href="#">
									<img src="{{ asset('media/logos/logo-5.png')}}">
								</a>
							</div>
							<div class="kt-login__signin">
							<div class="kt-login__head">
									<h3 class="kt-login__title">{{ __('Kayıt Ol') }}</h3>
									<div class="kt-login__desc">Hesap oluşturmak için bilgileri giriniz:</div>
								</div>
                                <form method="POST" class="kt-form" action="{{ route('register') }}">
                                 @csrf
									<div class="input-group">
                                        <input id="name" class="form-control @error('name') is-invalid @enderror" type="text" placeholder="{{ __('Ad') }}" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>
									<div class="input-group">
                                        <input id="email" class="form-control  @error('email') is-invalid @enderror" type="text" placeholder="{{ __('E-Mail Adresi') }}" name="email"  value="{{ old('email') }}" required autocomplete="off">

                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
									</div>
									<div class="input-group">
                                        <input id="password" class="form-control @error('password') is-invalid @enderror" type="password" placeholder="{{ __('Şifre') }}" name="password"  required autocomplete="new-password">
                                        
                                        @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror

									</div>
									<div class="input-group">
                                        <input id="password-confirm" class="form-control" type="password" placeholder="{{ __('Şifre Tekrarı') }}"  name="password_confirmation" required autocomplete="new-password">
									</div>
									<div class="row kt-login__extra">
										<div class="col kt-align-left">
											<label class="kt-checkbox">
												<input type="checkbox" name="agree">Şartlar ve koşulları <a href="#" class="kt-link kt-login__link kt-font-bold"> kabul ediyorum</a>.
												<span></span>
											</label>
											<span class="form-text text-muted"></span>
										</div>
									</div>
									<div class="kt-login__actions">
										<button class="btn btn-brand btn-elevate kt-login__btn-primary"> {{ __('Kayıt Ol') }}</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
@endsection
