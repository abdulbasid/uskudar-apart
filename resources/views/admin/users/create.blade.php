@extends('layouts.inside')

@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
    <div class="row">
        <div class="col-lg-12">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head kt-portlet__head--lg">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">Ekle <small>Kullanıcı</small></h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <a href="001-datatable.html" class="btn btn-clean kt-margin-r-10">
                            <i class="la la-arrow-left"></i>
                            <span class="kt-hidden-mobile">Geri</span>
                        </a>
                        <div class="btn-group">
                            <button type="button" class="btn btn-brand">
                                <i class="la la-check"></i>
                                <span class="kt-hidden-mobile">Kaydet</span>
                            </button>
                            <button type="button" class="btn btn-brand dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-reload"></i>
                                            <span class="kt-nav__link-text">Save & continue</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-power"></i>
                                            <span class="kt-nav__link-text">Save & exit</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-edit-interface-symbol-of-pencil-tool"></i>
                                            <span class="kt-nav__link-text">Save & edit</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon flaticon2-add-1"></i>
                                            <span class="kt-nav__link-text">Save & add new</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form kt-form--label-right">
                    <div class="kt-portlet__body">
                        <div class="form-group row form-group-marginless kt-margin-t-20">
                            <label class="col-lg-1 col-form-label">Kullanıcı Adı:</label>
                            <div class="col-lg-3">
                                <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text"><i class="la la-user"></i></span></div>
                                    <input type="text" class="form-control" placeholder="">
                                </div>
                                <span class="form-text text-muted">Lütfen Kullanıcı Adı Giriniz</span>
                            </div>

                            <label class="col-lg-1 col-form-label">Email:</label>
                            <div class="col-lg-3">
                                <div class="form-group ">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">@</span></div>
                                            <input type="email" class="form-control" placeholder="Email">
                                        </div>
                                        <span class="form-text text-muted">Lütfen Email Adresi Yazınız</span>
                                    </div>
                            </div>

                            <label class="col-lg-1 col-form-label">Durumu:</label>
                            <div class="col-lg-3">
                                    <div class="form-group">
                                            <select class="form-control" id="exampleSelectd">
                                                <option vlaue="0">Seçiniz</option>
                                                <option vlaue="1">Aktif</option>
                                                <option value="2">Pasif</option>
                                            </select>
                                            <span class="form-text text-muted">Durum Seçiniz</span>
                                        </div>
                            </div>

                            
                            
                        </div>
                        <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg kt-separator--portlet-fit"></div>
                        
                        <div class="form-group row">
                            <label class="col-lg-1 col-form-label">Rol:</label>
                            <div class="col-lg-3">
                                <div class="kt-radio-inline">
                                    <label class="kt-radio kt-radio--solid">
                                        <input type="radio" name="example_2" checked value="2"> Yönetici
                                        <span></span>
                                    </label>
                                    <label class="kt-radio kt-radio--solid">
                                        <input type="radio" name="example_2" value="2"> Admin
                                        <span></span>
                                    </label>
                                    <label class="kt-radio kt-radio--solid">
                                        <input type="radio" name="example_2" value="2"> Temsilci
                                        <span></span>
                                    </label>
                                    <label class="kt-radio kt-radio--solid">
                                        <input type="radio" name="example_2" value="2"> Finans
                                        <span></span>
                                    </label>
                                </div>
                                <span class="form-text text-muted">Lütfen Rolü seçiniz</span>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <div class="row">
                                
                                <div class="col-lg-7">
                                    <button type="reset" class="btn btn-brand">Kaydet</button>
                                    <button type="reset" class="btn btn-secondary">Temizle</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->
        </div>
    </div>
</div>
@endsection