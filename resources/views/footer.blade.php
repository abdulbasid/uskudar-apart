@extends('layouts.inside')

@section('content')
<div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop">
    <div class="kt-footer__copyright">
        2019&nbsp;&copy;&nbsp;<a href="http://keenthemes.com/metronic" target="_blank" class="kt-link">ABY</a>
    </div>
    <div class="kt-footer__menu">
        <a href="{{ route('front.about-us')}}" target="_blank" class="kt-footer__menu-link kt-link">Hakkımızda</a>
        <a href="{{ route('front.about-apart')}}" target="_blank" class="kt-footer__menu-link kt-link">Apart Hakkında</a>
        <a href="{{ route('front.contact')}}" target="_blank" class="kt-footer__menu-link kt-link">İletişim</a>
    </div>
</div>
@endsection


          