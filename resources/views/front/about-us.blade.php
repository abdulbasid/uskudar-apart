@extends('layouts.outside')

@section('content')
<div id="Content">
            <div class="content_wrapper clearfix">
                <div class="sections_group">
                    <div class="entry-content">
                        <div class="section" style="padding-top:100px; padding-bottom:60px; background-image:url(images/home_developer_photo_subheader.jpg); background-repeat:no-repeat; background-position:center top; background-attachment:fixed;">
                            <div class="section_wrapper clearfix">
                                <div class="items_group clearfix">
                                    <!-- One full width row-->
                                    <div class="column one column_column ">
                                        <div class="column_attr align_center">
                                            <h2 class="subheader">Hakkımızda</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="section column-margin-20px" style="padding-top:40px; padding-bottom:10px; ">
                            <div class="section_wrapper clearfix">
                                <div class="items_group clearfix">
                                    <!-- One full width row-->
                                    <div class="column one column_column ">
                                        <div class="column_attr align_center">
                                            <div class="image_frame no_link scale-with-grid aligncenter no_border">
                                                <div class="image_wrapper"><img class="scale-with-grid" src="{{ asset('images/home_developer_thumb_slider_icon_1.png') }}" alt="" />
                                                </div>
                                            </div>
                                            @foreach($aboutuses as $aboutus)
                                            <h2 style="text-align: center;">{{$aboutus->title}}</h2>
                                            @endforeach
                                        </div>
                                    </div>
                                    <!-- One Second (1/2) Column -->
                                    <div class="column one-second column_image ">
                                        <div class="image_frame no_link scale-with-grid aligncenter no_border">
                                            <div class="image_wrapper"><img class="scale-with-grid" src="{{ asset('images/home_developer_thumb_slider_1.png') }}" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                    <!-- One Second (1/2) Column -->
                                    <div class="column one-second column_column ">
                                        <div class="column_attr ">
                                            <hr class="no_line hrmargin_b_20" />
                                            @foreach($aboutuses as $aboutus)
                                            <!-- One Second (1/2) Column -->
                                            <div class="column one-second">
                                            
                                                <p class="big">
                                                {{$aboutus->paragraph1}}
                                                </p>
 
                                            </div>
                                            <!-- One Second (1/2) Column -->
                                            <div class="column one-second">
                                                <p class="big">
                                                {{$aboutus->paragraph2}}
                                                </p>
                                              
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="section the_content no_content">
                            <div class="section_wrapper">
                                <div class="the_content_wrapper"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection