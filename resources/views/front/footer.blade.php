
<footer id="Footer" class="clearfix">
    <!-- Footer - First area -->
    <div class="footer_action">
        <div class="container">
            <!-- One full width row-->
            <div class="column one column_column">
                <a href="#" style="text-decoration: none; color: #CCCCCC;" target="_blank"><span style="font-size: 18px">Detaylı Bilgi Almak için <span class="themecolor">Üsküdar |</span><span style="color: #fff;"> Apart'ı</span> hemen arayın <span style="color: #fff;"><a href="{{ route('front.contact') }}">TIKLA!</a></span></span></a>
            </div>
        </div>
    </div>
    <div class="widgets_wrapper">
        <div class="container">
            <div class="one-fourth column">
                <!-- Text Area -->
                <aside id="text-7" class="widget widget_text">
                    <div class="textwidget"><img src="{{ asset('images/logo_footer.png') }}" alt="" width="130" height="30" />
                        <p>
                            <span class="big">Kendinizi mutlu, huzurlu ve güvende hissedebileceğiniz</span>
                            <span class="big">eviniz gibi benimseyeceğiniz, harika arkadaşlık</span>
                            <span class="big">bağlarının olduğu eğlenceli,neşeli bir apart.</span>
                        </p>
                        <p>
                            
                        </p> 
                        <a href="https://facebook.com/" target="blank" class="icon_bar icon_bar_facebook icon_bar_small"><span class="t">
                            <i class="fab fa-facebook-f"></i></span><span class="b">
                            <i class="fab fa-facebook-f"></i></span>
                        </a>

                        <a href="https://twitter.com/" target="blank" class="icon_bar icon_bar_twitter icon_bar_small"><span class="t">
                            <i class="fab fa-twitter"></i></span><span class="b">
                            <i class="fab fa-twitter"></i></span>
                        </a>

                        <a href="https://www.youtube.com/watch?v=3bWaJsOzRK4?autoplay=0" target="blank" class="icon_bar icon_bar_youtube icon_bar_small"><span class="t">
                            <i class="fab fa-youtube"></i></span><span class="b">
                            <i class="fab fa-youtube"></i></span>
                        </a>
                    </div>
                </aside>
            </div>
            <div class="one-fourth column">
                <!-- Recent Comments Area -->
                <aside class="widget widget_mfn_recent_comments">
                    <h4>Sayfalar</h4>
                    <div class="Recent_comments">
                        <ul class="menu">
                            <li>
                                <span class="date_label"><a href="{{ route('front.about-us')}}">Hakkımızda</a></span>
                            </li>
                            <li>
                                <span class="date_label"><a href="{{ route('front.about-apart')}}">Apart Hakkında</a></span>
                            </li>
                            <li>
                                <span class="date_label"><a href="{{ route('front.galery')}}">Galeri</a></span>
                            </li>
                            <li>
                                <span class="date_label"><a href="{{ route('front.contact')}}">İletişim</a></span>
                            </li>
                        </ul>
                    </div>
                </aside>
            </div>
            <div class="one-fourth column">
                <!-- Recent posts -->
                <aside class="widget widget_mfn_recent_posts">
                    <h4>Çamaşır Yıkama</h4>
                    <div class="Recent_posts">
                        <ul>
                            <li class="post ">
                                <a href="blog-single-content-builder.html">
                                    <div class="photo"><img width="80" height="80" src="{{ asset('images/beauty_portfolio_2-80x80.jpg') }}" class="scale-with-grid wp-post-image" alt="beauty_portfolio_2" /><span class="c"><i class="fas fa-check"></i></span>
                                    </div>
                                    <div class="desc">
                                        <h6>Haftaiçi</h6><span class="date"><i class="far fa-clock"></i> 07:00 - 22:00</span>
                                    </div>
                                </a>
                            </li>
                            <li class="post format-image">
                                <a href="blog-single-vertical-photo.html">
                                    <div class="photo"><img width="80" height="80" src="{{ asset('images/blog_vertical-80x80.jpg') }}" class="scale-with-grid wp-post-image" alt="blog_vertical" /><span class="c"><i class="fas fa-check"></i></span>
                                    </div>
                                    <div class="desc">
                                        <h6>Haftasonu</h6><span class="date"><i class="far fa-clock"></i>07:00 - 23:59</span>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </aside>
            </div>
            <div class="one-fourth column">
                <!-- Text Area -->
                <aside id="text-8" class="widget widget_text">
                    <h4>ÖNEMLİ BİLGİLENDİRME</h4>
                    <div class="textwidget">
                        <ul class="list_mixed">
                            <li class="list_check">
                                Apartımıza Öğrenci Alınmamaktadır.
                            </li>
                            <li class="list_star">
                                Depozito, Komiyon, Kapora Yok.
                            </li>
                            <li class="list_idea">
                                Her Yere Ulaşım Kolaylığı.
                            </li>
                            <li class="list_check">
                               En Az Bir Aylık Konaklama İmkanı.
                            </li>
                        </ul>
                    </div>
                </aside>
            </div>
        </div>
    </div>
    <!-- Footer copyright-->
    <div class="footer_copy">
        <div class="container">
            <div class="column one">
                <a id="back_to_top" href="#" class="button button_left button_js"><i class="fas fa-chevron-up fa-2x"></i></a>
                <div class="copyright">
                    &copy; 2019 ABY - Developer <a href="https://www.linkedin.com/in/abyener/" target="_blank">Abdulbasid Yener</a>
                </div>
                <!--Social info area-->
                <ul class="social">
                    <li class="facebook">
                        <a href="https://www.facebook.com/" title="Facebook"><i class="fab fa-facebook-f"></i></a>
                    </li>
                   
                    <li class="twitter">
                        <a href="https://twitter.com/" title="Twitter"><i class="fab fa-twitter"></i></a>
                    </li>
                    <li class="youtube">
                        <a href="https://www.youtube.com/watch?v=3bWaJsOzRK4?autoplay=0" title="YouTube"><i class="fab fa-youtube"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
