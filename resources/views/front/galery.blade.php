@extends('layouts.outside')

@section('content')
<div id="Content">
    <div class="content_wrapper clearfix">
        <div class="sections_group">
            <div class="entry-content">
                <div class="section" style="padding-top:100px; padding-bottom:60px; background-image:url(images/home_developer_photo_subheader.jpg); background-repeat:no-repeat; background-position:center top; background-attachment:fixed;">
                    <div class="section_wrapper clearfix">
                        <div class="items_group clearfix">
                            <!-- One full width row-->
                            <div class="column one column_column ">
                                <div class="column_attr align_center">
                                    <h2 class="subheader">Fotoğraf Galerii</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section column-margin-20px" style="padding-top:40px; padding-bottom:30px; ">
                    <div class="section_wrapper clearfix">
                        <div class="items_group clearfix">
                            <!-- One Sixth (1/6) Column -->
                            <div class="column one-sixth column_placeholder">
                                <div class="placeholder">
                                    &nbsp;
                                </div>
                            </div>
                            <!-- Two Third (2/3) Column -->
                            <div class="column two-third column_column ">
                                <div class="column_attr align_center">
                                    <p style="font-size: 17px; line-height: 30px; font-style: italic; color: #F1B000;">
                                       Üsküdar Apart olarak en uygun, en rahat hizmeti ile her zaman arkadaşlarımızın yanındayız.
                                    </p>
                                </div>
                            </div>
                            <!-- One Fourth (1/4) Column -->
                            <div class="column one-fourth column_column ">
                                <div class="column_attr align_right">
                                    <hr class="no_line hrmargin_b_15">
                                    <h3>Dış Görüntü - Ortak Alanlar</h3>
                                    <p class="big">
                                      Apartamızın ortak alanları bulunmaktadır. Bunlar her katta 2 tuvalet, mutfak ve oturma salonu. En üst katta ise herkesin kullanabildiği deniz manzaralı terasımız bulunmaktadır.
                                    </p>
                                </div>
                            </div>
                            <!-- Three Fourth (3/4) Column -->
                            <div class="column three-fourth column_column ">
                                <div class="column_attr ">

                                    <!-- Image Gallery-->
                                    <div id='gallery-1' class='gallery galleryid-54 gallery-columns-3 gallery-size-thumbnail '>
                                        <!-- Gallery item -->
                                        <dl class='gallery-item'>
                                            <dt class='gallery-icon landscape'>
                                                    <a href='images/home_developer_photo_slider_4.jpg'><img width="300" height="300" src="images/home_developer_photo_slider_4-300x300.jpg" class="attachment-thumbnail" alt="home_developer_photo_slider_4" /></a>
                                                </dt>
                                            <dd></dd>
                                        </dl>
                                        <!-- Gallery item -->
                                        <dl class='gallery-item'>
                                            <dt class='gallery-icon landscape'>
                                                    <a href='images/home_developer_photo_slider_5.jpg'><img width="300" height="300" src="images/home_developer_photo_slider_5-300x300.jpg" class="attachment-thumbnail" alt="home_developer_photo_slider_5" /></a>
                                                </dt>
                                            <dd></dd>
                                        </dl>
                                        <!-- Gallery item -->
                                        <dl class='gallery-item'>
                                            <dt class='gallery-icon landscape'>
                                                    <a href='images/home_developer_photo_slider_3.jpg'><img width="300" height="300" src="images/home_developer_photo_slider_3-300x300.jpg" class="attachment-thumbnail" alt="home_developer_photo_slider_3" /></a>
                                                </dt>
                                            <dd></dd>
                                        </dl>
                                        <br class="flv_clear_both" />
                                        <!-- Gallery item -->
                                        <dl class='gallery-item'>
                                            <dt class='gallery-icon landscape'>
                                                    <a href='images/home_developer_photo_slider_2.jpg'><img width="300" height="300" src="images/home_developer_photo_slider_2-300x300.jpg" class="attachment-thumbnail" alt="home_developer_photo_slider_2" /></a>
                                                </dt>
                                            <dd></dd>
                                        </dl>
                                        <!-- Gallery item -->
                                        <dl class='gallery-item'>
                                            <dt class='gallery-icon landscape'>
                                                    <a href='images/home_developer_photo_slider_1.jpg'><img width="300" height="300" src="images/home_developer_photo_slider_1-300x300.jpg" class="attachment-thumbnail" alt="home_developer_photo_slider_1" /></a>
                                                </dt>
                                            <dd></dd>
                                        </dl>
                                        <br style='clear: both' />
                                    </div>
                                </div>
                            </div>
                            <!-- Page devider -->
                            <!-- One full width row-->
                            <div class="column one column_divider ">
                                <hr class="no_line" />
                            </div>
                            <!-- One Fourth (1/4) Column -->
                            <div class="column one-fourth column_column ">
                                <div class="column_attr align_right">
                                    <hr class="no_line hrmargin_b_15">
                                    <h3>Odalar</h3>
                                    <p class="big">
                                       İster tek kişilik oda, ister 2, 3, 4 kişilil odalarımız mevcuttur.
                                    </p>
                                </div>
                            </div>
                            <!-- Three Fourth (3/4) Column -->
                            <div class="column three-fourth column_column ">
                                <div class="column_attr ">

                                    <!-- Image Gallery-->
                                    <div id='gallery-2' class='gallery galleryid-54 gallery-columns-3 gallery-size-thumbnail '>
                                        <!-- Gallery item -->
                                        <dl class='gallery-item'>
                                            <dt class='gallery-icon landscape'>
                                                    <a href='images/home_developer_photo_slider_1.jpg'><img width="300" height="300" src="images/home_developer_photo_slider_1-300x300.jpg" class="attachment-thumbnail" alt="home_developer_photo_slider_1" /></a>
                                                </dt>
                                            <dd></dd>
                                        </dl>
                                        <!-- Gallery item -->
                                        <dl class='gallery-item'>
                                            <dt class='gallery-icon landscape'>
                                                    <a href='images/home_developer_photo_slider_2.jpg'><img width="300" height="300" src="images/home_developer_photo_slider_2-300x300.jpg" class="attachment-thumbnail" alt="home_developer_photo_slider_2" /></a>
                                                </dt>
                                            <dd></dd>
                                        </dl>
                                        <!-- Gallery item -->
                                        <dl class='gallery-item'>
                                            <dt class='gallery-icon landscape'>
                                                    <a href='images/home_developer_photo_slider_4.jpg'><img width="300" height="300" src="images/home_developer_photo_slider_4-300x300.jpg" class="attachment-thumbnail" alt="home_developer_photo_slider_4" /></a>
                                                </dt>
                                            <dd></dd>
                                        </dl>
                                        <br class="flv_clear_both" />
                                        <!-- Gallery item -->
                                        <dl class='gallery-item'>
                                            <dt class='gallery-icon landscape'>
                                                    <a href='images/home_developer_photo_slider_5.jpg'><img width="300" height="300" src="images/home_developer_photo_slider_5-300x300.jpg" class="attachment-thumbnail" alt="home_developer_photo_slider_5" /></a>
                                                </dt>
                                            <dd></dd>
                                        </dl>
                                        <!-- Gallery item -->
                                        <dl class='gallery-item'>
                                            <dt class='gallery-icon landscape'>
                                                    <a href='images/home_developer_photo_slider_3.jpg'><img width="300" height="300" src="images/home_developer_photo_slider_3-300x300.jpg" class="attachment-thumbnail" alt="home_developer_photo_slider_3" /></a>
                                                </dt>
                                            <dd></dd>
                                        </dl>
                                        <br style='clear: both' />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section the_content no_content">
                    <div class="section_wrapper">
                        <div class="the_content_wrapper"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection