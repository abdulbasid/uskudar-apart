@extends('layouts.outside')

@section('content')

<div id="Content">
    <div class="content_wrapper clearfix">
        <div class="sections_group">
            <div class="entry-content">
                <div class="section" style="padding-top:100px; padding-bottom:60px; background-image:url(images/home_developer_photo_subheader.jpg); background-repeat:no-repeat; background-position:center top; background-attachment:fixed;">
                    <div class="section_wrapper clearfix">
                        <div class="items_group clearfix">
                            <!-- One full width row-->
                            <div class="column one column_column ">
                                <div class="column_attr align_center">
                                    @foreach($aboutaparts as $aboutapart)
                                    <h2 class="subheader"> {{$aboutapart->title}}</h2>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section column-margin-20px" style="padding-top:40px; padding-bottom:30px; ">
                    <div class="section_wrapper clearfix">
                        <div class="items_group clearfix">
                            <!-- One Sixth (1/6) Column -->
                            <div class="column one-sixth column_placeholder">
                                <div class="placeholder">
                                    &nbsp;
                                </div>
                            </div>
                            <!-- Two Third (2/3) Column -->
                            <div class="column two-third column_column ">
                                <div class="column_attr align_center">
                                    @foreach($aboutaparts as $aboutapart)
                                    <p style="font-size: 17px; line-height: 30px; font-style: italic; color: #F1B000;">
                                        {{$aboutapart->paragraph}} 
                                    </p>
                                    @endforeach
                                </div>
                            </div>
                            @foreach($aboutaparts as $aboutapart)
                            <!-- One Second (1/2) Column -->
                            <div class="column one-second column_column ">
                                <div class="column_attr ">
                                    <p class="big">
                                    {{$aboutapart->paragraph1}} 
                                    </p>
                                </div>
                            </div>
                            <!-- One Second (1/2) Column -->
                            <div class="column one-second column_column ">
                                <div class="column_attr ">
                                    <p>
                                    {{$aboutapart->paragraph2}} 
                                    </p>
                                </div>
                            </div>
                            @endforeach
                            

                            @include('front.rooms')
                        </div>
                    </div>
                </div>
                <div class="section the_content no_content">
                    <div class="section_wrapper">
                        <div class="the_content_wrapper"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection