@extends('layouts.outside')

@section('content')
<div id="Content">
	<div class="content_wrapper clearfix">
		<div class="sections_group">
			<div class="entry-content">
				<div class="section" style="padding-top:100px; padding-bottom:60px; background-image:url(images/home_developer_photo_subheader.jpg); background-repeat:no-repeat; background-position:center top; background-attachment:fixed;">
					<div class="section_wrapper clearfix">
						<div class="items_group clearfix">
							<!-- One full width row-->
							<div class="column one column_column ">
								<div class="column_attr align_center">
								@foreach($contacts as $contact)
									<h2 class="subheader">{{$contact->title}} </h2>
								@endforeach
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="section full-width sections_style_0 ">
					<div class="section_wrapper clearfix">
						<div class="items_group clearfix">
							<!-- One full width row-->
							<div class="column one column_map ">

								<!-- Google map area -->
								<div class="google-map-wrapper no_border">
								<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d376.2543259229571!2d29.017298!3d41.0244988!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14cab78be5dbaa03%3A0xc21b162757bb35b3!2sBosch!5e0!3m2!1str!2str!4v1563795477142!5m2!1str!2str" width="100%" height="410" frameborder="0" style="border:0"></iframe>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="section" style="padding-top:40px; padding-bottom:0px; background-color:#ffba00">
					<div class="section_wrapper clearfix">
						<div class="items_group clearfix">
							<!-- One full width row-->
							<div class="column one column_column ">
								<div class="column_attr align_center">
									@foreach($contacts as $contact)
										<h2 style="margin: 0; color: #fff;">Bizi Arayın {{$contact->phone}} </h2>
									@endforeach
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="section" style="padding-top:50px; padding-bottom:10px; background-image:url(images/home_developer_section_gallery.jpg); background-repeat:no-repeat; background-position:center; ">
					<div class="section_wrapper clearfix">
						<div class="items_group clearfix">
							<!-- One Fourth (1/4) Column -->
							<div class="column one-fourth column_column ">
								<div class="column_attr ">
									<h3>Üsküdar Apart</h4>
										@foreach($contacts as $contact)
										<p class="big">
										<strong>Adres:</strong> {{$contact->adress}} <br>
										<strong>E-Mail:</strong> {{$contact->email}}
										</p>
										@endforeach
										<table>
											<tr>
												<td>Pzt-Cuma</td><td><b>10:00</b></td><td><b>20:00</b></td>
											</tr>
											<tr>
												<td>Cmt</td><td><b>10:00</b></td><td><b>20:00</b></td>
											</tr>
											<tr>
												<td>Paz</td><td><b>10:00</b></td><td><b>20:00</b></td>
											</tr>
										</table>
									</div>
								</div>
								<!-- One Second (1/2) Column -->
								<div class="column one-second column_column ">
									<div class="column_attr " >
										<h3>Formu Doldur Hemen Arayalım</h4>
										<div id="contactWrapper" >
									<form id="contactform">
										<div class="column one-second">
											<input placeholder="Ad" type="text" name="name" id="name" size="40" aria-required="true" aria-invalid="false" />
										</div>
										<div class="column one-second">
											<input placeholder="E-Mail" type="email" name="email" id="email" size="40" aria-required="true" aria-invalid="false" />
										</div>
										<div class="column one-second">
											<input placeholder="Telefon" type="tel" name="tel" id="tel" size="40" aria-required="true" aria-invalid="false" />
										</div>
										<!-- <div class="column one">
											<input placeholder="Subject" type="text" name="subject" id="subject" size="40" aria-invalid="false" />
										</div> -->
										<div class="column one">
											<textarea placeholder="Mesaj" name="body" id="body" style="width:100%;" rows="10" aria-invalid="false"></textarea>
										</div>
										<div class="column one">
											<input type="button" value="Gönder" id="submit" onClick="return check_values();">
										</div>
									</form>
								</div>
									</div>
								</div>
								<!-- One Fourth (1/4) Column -->
								<div class="column one-fourth column_column ">
									<div class="column_attr " >
									@foreach($contacts as $contact)
										<h3>{{$contact->title}}</h4>
									
										<p class="big">
										{{$contact->paragraph}}
										</p>
										
									@endforeach
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="section the_content no_content">
						<div class="section_wrapper">
							<div class="the_content_wrapper"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection