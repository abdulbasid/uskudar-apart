<div class="column one column_column ">
    <div class="column_attr ">
        <hr class="no_line hrmargin_b_20" />
        <h4 style="text-align: center;">Odalar Hakkında Bilgi <span class="highlight" >Ü</span></h4>
        <hr class="no_line hrmargin_b_30" />
        <table>
            <thead>
                <tr>
                    <th>Sıra</th>
                    <th>Oda Sayısı</th>
                    <th>Yatak Sayısı</th>
                    <th>Dolap Sayısı</th>
                    <th>Buzdolabı Sayısı</th>
                    <th>Klima</th>
                    <th>Çamaşır Makinası</th>
                    <th>Televizyon</th>
                    <th>Mutfak</th>
                    <th>Tuvalet/Banyo</th>
                    <th>Ücret</th>
                    <th>Görüntü</th>
                </tr>
            </thead>
            <tbody>
            @foreach($roomdets as $roomdet) 
                <tr>
                    <td>{{$roomdet->id}}</td>
                    <td>{{$roomdet->room}}</td>
                    <td>{{$roomdet->beds}}</td>
                    <td>{{$roomdet->cabinets}}</td>
                    <td>{{$roomdet->refrigerator}}</td>
                    <td>{{$roomdet->klima}}</td>
                    <td>{{$roomdet->washing_machine}}</td>
                    <td>{{$roomdet->television}}</td>
                    <td>{{$roomdet->kitchen}}</td>
                    <td>{{$roomdet->bathroom}}</td>
                    <td>{{$roomdet->price}}</td>
                    <td>
                        <a href="{{ route('front.galery')}}" target="blank"><i class="far fa-images fa-3x"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>