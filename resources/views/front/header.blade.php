
<div id="Header_creative">
    <a href="#" class="creative-menu-toggle"><i class="fas fa-bars"></i></a>
    <!--Social info area-->
    <ul class="social creative-social">
        
        <li class="facebook">
            <a target="_blank" href="https://facebook.com/" title="Facebook"><i class="fab fa-facebook-f"></i></a>
        </li>
       
        <li class="twitter">
            <a target="_blank" href="https://twitter.com/" title="Twitter"><i class="fab fa-twitter"></i></a>
        </li>
       
        <li class="youtube">
            <a target="_blank" href="https://www.youtube.com/watch?v=3bWaJsOzRK4?autoplay=0" title="YouTube"><i class="fab fa-youtube"></i></a>
        </li>
      
    </ul>
    <div class="creative-wrapper">
        <!-- Header -  Logo and Menu area -->
        <div id="Top_bar">
            <div class="one">
                <div class="top_bar_left">
                    <!-- Logo-->
                    <div class="logo">
                        <a id="logo" href="{{ route('welcome') }}" title="Üsküdar Apart"><img class="scale-with-grid" src="{{ asset('images/developer.png') }}" alt="BeDeveloper - BeTheme" />
                        </a>
                    </div>
                    <!-- Main menu-->
                    <div class="menu_wrapper">
                        <nav id="menu">
                            <ul id="menu-main-menu" class="menu">
                                <li class="current_page_item">
                                    <a href="{{ route('welcome') }}"><span>Anasayfa</span></a>
                                </li>
                                <li>
                                    <a href="{{ route('front.about-us') }}"><span>Hakkımızda</span></a>
                                </li>

                                <li>
                                    <a href="{{ route('front.about-apart') }}"><span>Apart Hakkında</span></a>
                                </li>
                                <li>
                                    <a href="{{ route('front.galery') }}"><span>Galeri</span></a>
                                </li>
                                <li>
                                    <a href="{{ route('front.contact') }}"><span>İletişim</span></a>
                                </li>
                            </ul>
                        </nav><a class="responsive-menu-toggle" href="#"><i class="fas fa-bars"></i></a>
                    </div>
                    <!-- Header Searchform area-->
                    <div class="search_wrapper">
                        <form method="get" id="searchform" action="#">
                            <i class="icon_search icon-search"></i><a href="#" class="icon_close"><i class="icon-cancel"></i></a>
                            <input type="text" class="field" name="s" id="s" placeholder="Enter your search" />
                            <input type="submit" class="submit flv_disp_none" value="" />
                        </form>
                    </div>
                </div>
                <!-- Banner area - only for certain pages-->
                <div class="banner_wrapper"></div>
            </div>
        </div>
        <!-- Header Top -  Info Area -->
        <div id="Action_bar">
            <!--Social info area-->
            <ul class="social">
                <li class="facebook">
                    <a target="_blank" href="https://www.facebook.com/" title="Facebook"><i class="fab fa-facebook-f"></i></a>
                </li>
                
                <li class="twitter">
                    <a target="_blank" href="https://www.twitter.com/" title="Twitter"><i class="fab fa-twitter"></i></a>
                </li>
               
                <li class="youtube">
                    <a target="_blank" href="https://www.youtube.com/watch?v=3bWaJsOzRK4?autoplay=0" title="YouTube"><i class="fab fa-youtube"></i></a>
                </li>
        
            </ul>
        </div>
    </div>
</div>