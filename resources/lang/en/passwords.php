<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Şifre en az 8 karekterli olmalı ve şifre tekrarı ile eşleşmeli.',
    'reset' => 'Şifreniz Sıfırlandı!',
    'sent' => 'Şifre sıfırlama bağlantınızı e-postayla gönderdik!',
    'token' => 'This password reset token is invalid.',
    'user' => "Bu e-posta adresine ait bir kullanıcı bulunamadı.",

];
