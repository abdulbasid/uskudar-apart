<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenants', function (Blueprint $table) {
            $table->bigIncrements('id');
            
            $table->string('tc');
            $table->string('name')->nullable();
            $table->string('surname')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('job')->nullable();
            $table->float('price')->nullable();
            $table->string('startdate')->nullable();
            $table->string('enddate')->nullable();
            $table->unsignedBigInteger('floor_id');
            $table->unsignedBigInteger('room_id');
            $table->enum('statu', ['Kalıyor','Ayrıldı'])->nullable();
            

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('floor_id')->references('id')->on('floors');
            $table->foreign('room_id')->references('id')->on('rooms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenants');
    }
}
