<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomDetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_dets', function (Blueprint $table) {
            $table->bigIncrements('id');

            // $table->unsignedInteger('floor_id');
            $table->string('room')->nullable();
            $table->integer('beds')->nullable();
            $table->integer('cabinets')->nullable();
            $table->integer('refrigerator')->nullable();
            $table->integer('klima')->nullable();;
            $table->string('washing_machine')->nullable();

            $table->string('television')->nullable();
            $table->string('kitchen')->nullable();
            $table->string('bathroom')->nullable();
            $table->string('price')->nullable();

            $table->timestamps();
            $table->softDeletes();


            // $table->foreign('floor_id')->references('id')->on('floors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_dets');
    }
}
