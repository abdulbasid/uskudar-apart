<?php

use Illuminate\Database\Seeder;

class LocationItemsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('location_items')->delete();
        
        \DB::table('location_items')->insert(array (
            0 => 
            array (
                'id' => 1,
                'item' => 'Marmaraya 2 Dakikalık Yürüme Mesafesinde.',
                'created_at' => '2019-07-25 11:30:41',
                'updated_at' => '2019-07-25 11:30:41',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'item' => 'Vapur İskelesine 3 Dakikalık Yürüme Mesafesinde.',
                'created_at' => '2019-07-25 11:30:53',
                'updated_at' => '2019-07-25 11:30:53',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'item' => 'Metroya 3 Dakikalık Yürüme Mesafesinde.',
                'created_at' => '2019-07-25 11:31:05',
                'updated_at' => '2019-07-25 11:31:05',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'item' => 'Otobüs, Minibüs ve Taksi Durakalrına 2 Dakikalık Yürüme Mesafesinde.',
                'created_at' => '2019-07-25 11:31:14',
                'updated_at' => '2019-07-25 11:31:14',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}