<?php

use Illuminate\Database\Seeder;

class SourcesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('sources')->delete();
        
        \DB::table('sources')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Sistem',
                'slug' => 'sistem',
                'code' => 'SYSTEM',
                'created_at' => '2019-07-19 16:45:11',
                'updated_at' => '2019-07-19 16:45:11',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Facebook',
                'slug' => 'facebook',
                'code' => 'FACEBOOK',
                'created_at' => '2019-07-19 16:45:38',
                'updated_at' => '2019-07-19 16:45:38',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Instagram',
                'slug' => 'instagram',
                'code' => 'INSTAGRAM',
                'created_at' => '2019-07-19 16:46:15',
                'updated_at' => '2019-07-19 16:46:15',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}