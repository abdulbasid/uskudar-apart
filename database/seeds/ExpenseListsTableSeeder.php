<?php

use Illuminate\Database\Seeder;

class ExpenseListsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('expense_lists')->delete();
        
        \DB::table('expense_lists')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Elektirik',
                'created_at' => '2019-07-23 15:58:19',
                'updated_at' => '2019-07-23 15:58:19',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Su',
                'created_at' => '2019-07-23 15:58:31',
                'updated_at' => '2019-07-23 15:58:31',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Klima',
                'created_at' => '2019-07-23 15:58:41',
                'updated_at' => '2019-07-23 15:58:41',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Tadilat',
                'created_at' => '2019-07-23 15:58:49',
                'updated_at' => '2019-07-23 15:58:49',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Diğer',
                'created_at' => '2019-07-23 15:58:59',
                'updated_at' => '2019-07-23 15:58:59',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}