<?php

use Illuminate\Database\Seeder;

class CustomerStatusesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('customer_statuses')->delete();
        
        \DB::table('customer_statuses')->insert(array (
            0 => 
            array (
                'id' => 1,
                'slug' => 'islem-gormemis',
                'code' => 'PENDING',
                'name' => 'Islem Gormemis',
                'color' => 'yellow',
                'created_at' => '2019-07-19 16:47:15',
                'updated_at' => '2019-07-19 16:47:15',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'slug' => 'ulasilamadi',
                'code' => 'NOT_REACHED',
                'name' => 'Ulasilamadi',
                'color' => 'orange',
                'created_at' => '2019-07-19 16:48:39',
                'updated_at' => '2019-07-19 16:48:39',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'slug' => 'randevu-verildi',
                'code' => 'APPOINTMENT',
                'name' => 'Randevu Verildi',
                'color' => 'green',
                'created_at' => '2019-07-19 16:49:55',
                'updated_at' => '2019-07-19 16:49:55',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}