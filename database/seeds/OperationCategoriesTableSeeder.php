<?php

use Illuminate\Database\Seeder;

class OperationCategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('operation_categories')->delete();
        
        \DB::table('operation_categories')->insert(array (
            0 => 
            array (
                'id' => 1,
            'name' => 'Liposuction (Yag Aldirma)',
                'code' => 'LIPOSUCTION',
                'created_at' => '2019-07-19 16:51:24',
                'updated_at' => '2019-07-19 16:51:24',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Germe',
                'code' => 'STRETCHING',
                'created_at' => '2019-07-19 16:51:59',
                'updated_at' => '2019-07-19 16:51:59',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Burun Ameliyati',
                'code' => 'NOSE_JOB',
                'created_at' => '2019-07-19 16:53:03',
                'updated_at' => '2019-07-19 16:53:03',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Meme',
                'code' => 'BREAST',
                'created_at' => '2019-07-19 16:53:45',
                'updated_at' => '2019-07-19 16:53:45',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Eyelid',
                'code' => 'EYELID',
                'created_at' => '2019-07-19 16:54:27',
                'updated_at' => '2019-07-19 16:54:27',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Kas',
                'code' => 'EYEEBROW',
                'created_at' => '2019-07-19 16:55:06',
                'updated_at' => '2019-07-19 16:55:06',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Popo',
                'code' => 'BUTT',
                'created_at' => '2019-07-19 16:55:50',
                'updated_at' => '2019-07-19 16:55:50',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Vajen Daraltma',
                'code' => 'VAGINA_TIGHTENING',
                'created_at' => '2019-07-19 16:56:43',
                'updated_at' => '2019-07-19 16:56:43',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Labium Kucultme',
                'code' => 'LABIUM_SHRINKING',
                'created_at' => '2019-07-19 16:57:51',
                'updated_at' => '2019-07-19 16:57:51',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}