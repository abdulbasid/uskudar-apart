<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $role_manager  = Role::where('name', 'manager')->first();
        $role_admin  = Role::where('name', 'admin')->first();
       
        
        $manager = new User();
        $manager->name = 'Tahsin Batmaz';
        $manager->email = 'tahsin@batmaz.com';
        $manager->password = '$2y$10$CjsTUfKjHGYdyIq0H.VOLuevnmLxJl5x4oP94.g3g9YQXcclABYj2';
        $manager->save();
        $manager->roles()->attach($role_manager);
        
        $admin = new User();
        $admin->name = 'admin';
        $admin->email = 'admin@admin.com';
        $admin->password = '$2y$10$CjsTUfKjHGYdyIq0H.VOLuevnmLxJl5x4oP94.g3g9YQXcclABYj2';
        $admin->save();
        $admin->roles()->attach($role_admin);
    }
}
