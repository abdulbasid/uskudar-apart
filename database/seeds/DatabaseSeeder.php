<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
       // Role comes before User seeder here.
        $this->call(RoleTableSeeder::class);
        // User seeder will use the roles above created.
        $this->call(UserTableSeeder::class);

        $this->call(RoomDetsTableSeeder::class);
        $this->call(ExpenseListsTableSeeder::class);
        $this->call(ContactsTableSeeder::class);
        $this->call(AboutapartsTableSeeder::class);
        $this->call(AboutusesTableSeeder::class);
        $this->call(LocationItemsTableSeeder::class);
        $this->call(ApartPropertiesTableSeeder::class);
        $this->call(FloorsTableSeeder::class);
        $this->call(RoomsTableSeeder::class);
        $this->call(WhyapartsTableSeeder::class);
    }
}
