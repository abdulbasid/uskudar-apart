<?php

use Illuminate\Database\Seeder;

class WhyapartsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('whyaparts')->delete();
        
        \DB::table('whyaparts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Perfect localization in the centre with park nearby',
                'paragraph' => 'Suspendisse a pellentesque dui, non felis. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula. Ut molestie a, ultricies porta urna  Vestibulum commodo volutpat a, convallis ac, laoreet enim. Phasellus fermentum in, dolor. Pellentesque facilisis. Nulla imperdiet sit amet magna. Vestibulum dapibus, mauris nec malesuada fames ac turpis velit, rhoncus eu, luctus et interdum adipiscing wisi. Aliquam erat ac ipsum. Integer aliquam purus. Quisque lorem tortor fringilla sed, vestibulum id, eleifend justo vel bibendum sapien massa ac turpis faucibus orci luctus non',
                'photo' => '-',
                'created_at' => '2019-07-25 23:35:55',
                'updated_at' => '2019-07-25 23:35:55',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Building Managment System in every apartment',
                'paragraph' => 'Nullam et malesuada fames ac arcu. Suspendisse rutrum ligula, semper id, urna. Nulla nec neque. In ornare risus. Phasellus placerat id, cursus non, nulla. Duis ac lacus. Nulla mi quis lacus et  Integer mi ligula, nonummy velit odio condimentum ante. Fusce ullamcorper. Suspendisse in aliquam ut, eleifend ac, sodales wisi a arcu. Cras rhoncus eu, posuere eget, eros. Nullam laoreet. Proin porttitor ullamcorper, lorem id mi. Fusce tempor tristique, leo sit amet, consectetuer adipiscing eget, dui. Nullam euismod, nulla ipsum primis in faucibus eros diam elit justo',
                'photo' => '-',
                'created_at' => '2019-07-25 23:54:34',
                'updated_at' => '2019-07-25 23:54:34',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Air conditioner and electrical heat system',
                'paragraph' => 'Donec a nunc. Suspendisse est. Sed mauris ac lectus. Nullam augue pulvinar odio. Morbi tincidunt. Nulla facilisi. Morbi mauris lacus vestibulum vehicula  Nunc sit amet, consectetuer eget, ultricies iaculis et, posuere dui. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per conubia nostra, per inceptos hymenaeos. Fusce nulla eu scelerisque tincidunt. Praesent odio eu dui sed felis cursus. Maecenas nec tincidunt in, mollis aliquam, nulla luctus nulla nulla, egestas ipsum scelerisque vel, ipsum. Curabitur tempor. Quisque est ullamcorper lorem semper convallis.',
                'photo' => '-',
                'created_at' => '2019-07-25 23:56:20',
                'updated_at' => '2019-07-25 23:56:20',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'Feel the warmth and relaxing  ambiance of fireplace',
                'paragraph' => 'Praesent in eleifend in, dapibus aliquam. Nunc erat erat eu sem pede sit amet dignissim turpis. Mauris at sapien eu pede bibendum tempus. Suspendisse fermentum molestie vitae, facilisis dignissim massa.  Cum sociis natoque penatibus et luctus et magnis dis parturient montes, nascetur ridiculus mus. Morbi urna viverra neque, vitae leo. Donec a nunc. Suspendisse est. Sed mauris ac lectus. Nullam augue pulvinar odio. Morbi tincidunt. Nulla facilisi. Morbi mauris lacus  Maecenas nec tincidunt in, mollis aliquam, nulla luctus nulla nulla, egestas ipsum scelerisque vel, ipsum. Curabitur tempor. Quisque est ullamcorper lorem semper convallis.',
                'photo' => '-',
                'created_at' => '2019-07-25 23:56:56',
                'updated_at' => '2019-07-25 23:56:56',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}