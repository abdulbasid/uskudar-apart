<?php

use Illuminate\Database\Seeder;

class RoomDetsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('room_dets')->delete();
        
        \DB::table('room_dets')->insert(array (
            0 => 
            array (
                'id' => 1,
                'room' => 'Tek Kişilik Oda',
                'beds' => 1,
                'cabinets' => 1,
                'refrigerator' => 1,
                'klima' => 1,
                'washing_machine' => 'Ortak Alan',
                'television' => 'Ortak Alan',
                'kitchen' => 'Ortak Alan',
                'bathroom' => 'Ortak Alan',
                'price' => '0',
                'created_at' => '2019-07-23 12:21:05',
                'updated_at' => '2019-07-23 12:21:05',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'room' => 'Çift Kişilik Oda',
                'beds' => 2,
                'cabinets' => 2,
                'refrigerator' => 1,
                'klima' => 1,
                'washing_machine' => 'Ortak Alan',
                'television' => 'Ortak Alan',
                'kitchen' => 'Ortak Alan',
                'bathroom' => 'Ortak Alan',
                'price' => '0',
                'created_at' => '2019-07-23 12:25:22',
                'updated_at' => '2019-07-23 12:25:22',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'room' => 'Üç Kişilik Oda',
                'beds' => 3,
                'cabinets' => 3,
                'refrigerator' => 1,
                'klima' => 1,
                'washing_machine' => 'Ortak Alan',
                'television' => 'Ortak Alan',
                'kitchen' => 'Ortak Alan',
                'bathroom' => 'Ortak Alan',
                'price' => '0',
                'created_at' => '2019-07-23 12:25:44',
                'updated_at' => '2019-07-23 12:25:44',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'room' => 'Dört Kişilik Oda',
                'beds' => 4,
                'cabinets' => 4,
                'refrigerator' => 1,
                'klima' => 1,
                'washing_machine' => 'Ortak Alan',
                'television' => 'Ortak Alan',
                'kitchen' => 'Ortak Alan',
                'bathroom' => 'Ortak Alan',
                'price' => '0',
                'created_at' => '2019-07-23 12:26:05',
                'updated_at' => '2019-07-23 12:26:05',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}