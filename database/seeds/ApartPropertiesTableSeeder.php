<?php

use Illuminate\Database\Seeder;

class ApartPropertiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('apart_properties')->delete();
        
        \DB::table('apart_properties')->insert(array (
            0 => 
            array (
                'id' => 1,
                'order' => 1,
                'property' => '"Marmaraya" 2 Dakikalık Yürüme Mesafesinde.',
                'created_at' => '2019-07-25 11:53:58',
                'updated_at' => '2019-07-25 11:53:58',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'order' => 2,
                'property' => '"Vapur İskelesine" 3 Dakikalık Yürüme Mesafesinde.',
                'created_at' => '2019-07-25 11:54:08',
                'updated_at' => '2019-07-25 11:54:08',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'order' => 3,
                'property' => '"Metroya"  3 Dakikalık Yürüme Mesafesinde.',
                'created_at' => '2019-07-25 11:54:19',
                'updated_at' => '2019-07-25 11:54:19',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'order' => 4,
                'property' => '"Otobüs, Minibüs ve Taksi Durakalrına"  2 Dakikalık Yürüme Mesafesinde.',
                'created_at' => '2019-07-25 11:54:30',
                'updated_at' => '2019-07-25 11:54:30',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'order' => 5,
                'property' => '"Neden Biz ?" Depozito Yok , Komisyon Yok , Kapora Yok',
                'created_at' => '2019-07-25 11:54:42',
                'updated_at' => '2019-07-25 11:54:42',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'order' => 6,
                'property' => '"Ücrette İndirim" Gazilere, Şehit Evlatlarına, Yetimlere, Emniyet...',
                'created_at' => '2019-07-25 11:54:53',
                'updated_at' => '2019-07-25 11:54:53',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'order' => 7,
                'property' => 'Aylık veya Yıllık Konaklama İmkanı" En az 1 ay şartı bulunmaktadır.',
                'created_at' => '2019-07-25 11:55:03',
                'updated_at' => '2019-07-25 11:55:03',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'order' => 8,
                'property' => '"Sahibinden Kiralama" Kiralama işlemi 6098 sayılı Kanunun\\\'na göre aylık olarak yapılmaktadır.',
                'created_at' => '2019-07-25 11:55:13',
                'updated_at' => '2019-07-25 11:55:13',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}