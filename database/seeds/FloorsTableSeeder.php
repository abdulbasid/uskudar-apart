<?php

use Illuminate\Database\Seeder;

class FloorsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('floors')->delete();
        
        \DB::table('floors')->insert(array (
            0 => 
            array (
                'id' => 1,
                'floor' => '1. Kat',
                'created_at' => '2019-07-25 13:00:02',
                'updated_at' => '2019-07-25 13:00:02',
            ),
            1 => 
            array (
                'id' => 2,
                'floor' => '2. Kat',
                'created_at' => '2019-07-25 13:00:09',
                'updated_at' => '2019-07-25 13:00:09',
            ),
            2 => 
            array (
                'id' => 3,
                'floor' => '3. Kat',
                'created_at' => '2019-07-25 13:00:15',
                'updated_at' => '2019-07-25 13:00:15',
            ),
            3 => 
            array (
                'id' => 4,
                'floor' => '4. Kat',
                'created_at' => '2019-07-25 13:00:21',
                'updated_at' => '2019-07-25 13:00:21',
            ),
        ));
        
        
    }
}