<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('contacts')->delete();
        
        \DB::table('contacts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'phone' => '0 555 555 55 55',
                'adress' => 'Mimar Sinan Mah. Selami Ali Efenedi cad. Şerifali Apartmanı No:11 Daire:3 Üsküdar/İstanbul',
                'email' => 'uskudarapart@info.com',
                'title' => 'Bizimle İletişime Geç',
                'paragraph' => 'Detaylı bilgi almak istiyorsanız bize bu formu doldurup yada telefonla arayarak iletişime geçebilirsiniz. Gün içinde dönüş yapmaktayız.',
                'created_at' => '2019-07-25 11:07:54',
                'updated_at' => '2019-07-25 11:07:54',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}