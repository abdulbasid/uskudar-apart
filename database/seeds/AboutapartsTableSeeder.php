<?php

use Illuminate\Database\Seeder;

class AboutapartsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('aboutaparts')->delete();
        
        \DB::table('aboutaparts')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Apart Hakkında',
                'paragraph' => 'Üsküdarın en güzel konumuna sahip , tüm ulaşım araçlarına kolaylıkla ulaşılabilecek. Teras ile deniz manzarası sunan harika bir yer.',
                'paragraph1' => 'Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat, velit mauris egestas quam, ut aliquam massa nisl quis neque. Suspendisse in orci enim. Telis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi. Proin condimentum fermentum nunc. Etiam pharetra, erat sed fermentum feugiat velit.  Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Nullam ac urna eu felis dapibus condimentum sit amet a augue. Sed non neque elit. Sed ut imperdiet nisi.',
                'paragraph2' => 'Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo. Suspendisse in orci enim.  Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris in erat justo.',
                'created_at' => '2019-07-25 10:31:02',
                'updated_at' => '2019-07-25 11:15:30',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}