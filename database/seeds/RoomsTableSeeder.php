<?php

use Illuminate\Database\Seeder;

class RoomsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('rooms')->delete();
        
        \DB::table('rooms')->insert(array (
            0 => 
            array (
                'id' => 1,
                'floor_id' => 1,
                'number' => 10,
                'roomCount' => 2,
                'remaining_number' => 2,
                'created_at' => '2019-07-25 13:35:27',
                'updated_at' => '2019-07-25 13:35:27',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'floor_id' => 1,
                'number' => 11,
                'roomCount' => 2,
                'remaining_number' => 2,
                'created_at' => '2019-07-25 13:35:46',
                'updated_at' => '2019-07-25 13:35:46',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'floor_id' => 1,
                'number' => 12,
                'roomCount' => 2,
                'remaining_number' => 2,
                'created_at' => '2019-07-25 13:36:00',
                'updated_at' => '2019-07-25 13:36:00',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'floor_id' => 1,
                'number' => 13,
                'roomCount' => 3,
                'remaining_number' => 1,
                'created_at' => '2019-07-25 13:36:13',
                'updated_at' => '2019-07-25 13:36:13',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'floor_id' => 2,
                'number' => 14,
                'roomCount' => 3,
                'remaining_number' => 0,
                'created_at' => '2019-07-25 13:36:37',
                'updated_at' => '2019-07-25 13:36:37',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'floor_id' => 2,
                'number' => 15,
                'roomCount' => 2,
                'remaining_number' => 2,
                'created_at' => '2019-07-25 13:36:52',
                'updated_at' => '2019-07-25 13:36:52',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'floor_id' => 2,
                'number' => 16,
                'roomCount' => 2,
                'remaining_number' => 2,
                'created_at' => '2019-07-25 13:37:08',
                'updated_at' => '2019-07-25 13:37:08',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'floor_id' => 2,
                'number' => 17,
                'roomCount' => 2,
                'remaining_number' => 2,
                'created_at' => '2019-07-25 13:37:24',
                'updated_at' => '2019-07-25 13:37:24',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'floor_id' => 3,
                'number' => 18,
                'roomCount' => 2,
                'remaining_number' => 2,
                'created_at' => '2019-07-25 13:37:39',
                'updated_at' => '2019-07-25 13:39:38',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'floor_id' => 3,
                'number' => 19,
                'roomCount' => 3,
                'remaining_number' => 3,
                'created_at' => '2019-07-25 13:37:52',
                'updated_at' => '2019-07-25 13:38:01',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'floor_id' => 3,
                'number' => 20,
                'roomCount' => 2,
                'remaining_number' => 2,
                'created_at' => '2019-07-25 13:38:15',
                'updated_at' => '2019-07-25 13:38:15',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'floor_id' => 3,
                'number' => 21,
                'roomCount' => 2,
                'remaining_number' => 2,
                'created_at' => '2019-07-25 13:38:52',
                'updated_at' => '2019-07-25 13:39:15',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'floor_id' => 3,
                'number' => 22,
                'roomCount' => 2,
                'remaining_number' => 2,
                'created_at' => '2019-07-25 13:39:52',
                'updated_at' => '2019-07-25 13:39:52',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'floor_id' => 4,
                'number' => 23,
                'roomCount' => 2,
                'remaining_number' => 2,
                'created_at' => '2019-07-25 13:40:09',
                'updated_at' => '2019-07-25 13:40:09',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'floor_id' => 4,
                'number' => 24,
                'roomCount' => 2,
                'remaining_number' => 2,
                'created_at' => '2019-07-25 13:40:27',
                'updated_at' => '2019-07-25 13:40:27',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'floor_id' => 4,
                'number' => 25,
                'roomCount' => 4,
                'remaining_number' => 3,
                'created_at' => '2019-07-25 13:40:41',
                'updated_at' => '2019-07-25 13:40:41',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}